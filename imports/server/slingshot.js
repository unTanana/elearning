import { Meteor } from 'meteor/meteor';
import { Slingshot } from 'meteor/edgee:slingshot';

// create connection to s3 videos bucket
Slingshot.createDirective("aws-s3-videosUpload", Slingshot.S3Storage, {
    AWSAccessKeyId: Meteor.settings.amazon.ACCESS_KEY,
    AWSSecretAccessKey: Meteor.settings.amazon.SECRET_KEY,
    bucket: Meteor.settings.amazon.video_bucket,
    region: Meteor.settings.amazon.region,
    acl: "public-read", // Access Control List -> Read Access to files

    allowedFileTypes: [
        "video/mp4" // allow only video mp4 files
    ],
    maxSize: null, //10 * 1024 * 1024, // 10 MB (use null for unlimited).

    authorize: function () {
        // allow all
        return true;
    },

    key: function (file) {
        //Store file into a directory by the user's username.
        const name = file.name;

        const trimExtension = name.slice(0, -4);
        const imageExtension = name.slice(-4);
        const trimup = trimExtension.replace(/([^a-z0-9]+)/gi, '-')

        // generate unique key
        function guid(trimup) {
            return trimup + "-" + 'xxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                const r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            });
        }

        let aws_file = guid(trimup) + imageExtension;

        aws_file = aws_file.replace(/\s+/g, '-').toLowerCase()
        return "app/" + aws_file;
    }
});

// create connection to s3 attachments bucket
Slingshot.createDirective("aws-s3-attachmentsUpload", Slingshot.S3Storage, {
    AWSAccessKeyId: Meteor.settings.amazon.ACCESS_KEY,
    AWSSecretAccessKey: Meteor.settings.amazon.SECRET_KEY,
    bucket: Meteor.settings.amazon.attachments_bucket,
    region: Meteor.settings.amazon.region,
    acl: "public-read", // Access Control List -> Read Access to files

    // allow all kinds of file types, except videos and images
    allowedFileTypes: [
        "application/pdf",
        "application/msword",
        "text/plain",
        "text/csv",
        "application/rtf",
        "application/x-rtf",
        "text/richtext",
        "application/x-iwork-pages-sffpages",
        "application/x-abiword",
        "application/vnd.ms-xpsdocument",
        "application/epub+zip",
        "application/x-mspublisher",
        "application/vnd.fujixerox.docuworks",
        "application/onenote",
        "application/vnd.rig.cryptonote",
        "application/vnd.lotus-notes",
        "application/onenote",
        "application/vnd.lotus-wordpro",
        "application/x-texinfo",
        "application/vnd.ms-word.document.macroenabled.12",
        "image/vnd.ms-modi",
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        "application/vnd.oasis.opendocument.text-web",
        "application/vnd.oasis.opendocument.text",
        "application/vnd.ms-excel",
        "application/vnd.svd",
        "application/msword",
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        "application/vnd.openxmlformats-officedocument.wordprocessingml.template",
        "application/vnd.ms-word.document.macroEnabled.12",
        "application/vnd.ms-word.template.macroEnabled.12",
        "application/vnd.ms-excel",
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        "application/vnd.openxmlformats-officedocument.spreadsheetml.template",
        "application/vnd.ms-excel.sheet.macroEnabled.12",
        "application/vnd.ms-excel.template.macroEnabled.12",
        "application/vnd.ms-excel.addin.macroEnabled.12",
        "application/vnd.ms-excel.sheet.binary.macroEnabled.12",
        "application/vnd.ms-powerpoint",
        "application/vnd.openxmlformats-officedocument.presentationml.presentation",
        "application/vnd.openxmlformats-officedocument.presentationml.template",
        "application/vnd.openxmlformats-officedocument.presentationml.slideshow",
        "application/vnd.ms-powerpoint.addin.macroEnabled.12",
        "application/vnd.ms-powerpoint.presentation.macroEnabled.12",
        "application/vnd.ms-powerpoint.template.macroEnabled.12",
        "application/vnd.ms-powerpoint.slideshow.macroEnabled.12"
    ],
    maxSize: 10 * 1024 * 1024, // 10 MB (use null for unlimited).

    authorize: function () {
        // allow all
        return true;
    },

    key: function (file) {
        //Store file into a directory by the user's username.
        const name = file.name;

        const trimExtension = name.slice(0, -4);
        const imageExtension = name.slice(-4);
        const trimup = trimExtension.replace(/([^a-z0-9]+)/gi, '-')

        // generate unique key
        function guid(trimup) {
            return trimup + "-" + 'xxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                const r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            });
        }

        let aws_file = guid(trimup) + imageExtension;

        aws_file = aws_file.replace(/\s+/g, '-').toLowerCase()
        return "app/" + aws_file;
    }
});

// create connection to s3 thumbnails bucket
Slingshot.createDirective("aws-s3-thumbnailsUpload", Slingshot.S3Storage, {
    AWSAccessKeyId: Meteor.settings.amazon.ACCESS_KEY,
    AWSSecretAccessKey: Meteor.settings.amazon.SECRET_KEY,
    bucket: Meteor.settings.amazon.thumbnails_bucket,
    region: Meteor.settings.amazon.region,
    acl: "public-read", // Access Control List -> Read Access to files

    // allow images
    allowedFileTypes: [
        "image/png",
        "image/jpeg",
    ],
    maxSize: 10 * 1024 * 1024, // 10 MB (use null for unlimited).

    authorize: function () {
        // allow all

        return true;
    },

    key: function (file) {
        // Store file into a directory by the user's username.
        const name = file.name;

        const trimExtension = name.slice(0, -4);
        const imageExtension = name.slice(-4);
        const trimup = trimExtension.replace(/([^a-z0-9]+)/gi, '-')

        // generate unique key
        function guid(trimup) {
            return trimup + "-" + 'xxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                const r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            });
        }

        let aws_file = guid(trimup) + imageExtension;

        aws_file = aws_file.replace(/\s+/g, '-').toLowerCase()
        return "app/" + aws_file;
    }
});