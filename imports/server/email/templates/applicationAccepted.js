// defines the template for accepted application email
export default ({acceptedLink}) => {
  return `
    <div>
      <span>
        You have been accepted. Click the link below to finish your registration.
      </span>
      <br>
      <a href=${acceptedLink}>Complete registration</a>
    </div> 
  `;
}