// defines the template for decliend application email
export default () => {
  return `
    <div>
      <span>
        Your application has been declined.
      </span>
    </div> 
  `;
}