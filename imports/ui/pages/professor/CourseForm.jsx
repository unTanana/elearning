import React, { Component } from 'react';
import CourseSchema from './course.schema.js';
import { Formik, FieldArray, ErrorMessage } from 'formik';
import { Input, FileInput, Select, SearchableSelect, Checkbox, DatePicker } from '/imports/ui/components/formik';
import { CATEGORIES, LEVELS } from '/imports/api/courses/values.js';

export default class CourseForm extends Component {
    // initial state of the course form
    state = {
        isLoading: true,
        edit: false,
        // initial values of the form
        initialValues: {
            name: '',
            description: '',
            level: LEVELS[0].value,
            videos: [],
            tags: [],
            category: CATEGORIES[0].value,
            courseThumbnail: '',
            price: 0.0,
            prerequisites: [],
            examDate: '',
            examRoom: ''
        },
        availablePrerequisites: []
    }

    // get course, if editing one
    componentDidMount() {
        const id = this.props.match.params.id;
        if (id) { // if editing a course, get it by it's id
            // actual call to the server, getting the course
            Meteor.call('getCourseById', id, (err, course) => {
                // save availablePrerequisites in the state, not the actual course object
                const { availablePrerequisites } = course;
                delete course.availablePrerequisites;

                if (!err) {
                    this.setState({
                        initialValues: course,
                        edit: true,
                        isLoading: false,
                        availablePrerequisites
                    });
                }
            })
        } else {
            // if not editing a course, get available prerequisites from the server
            Meteor.call('getAllPrerequisites', (err, res) => {
                this.setState({
                    isLoading: false,
                    availablePrerequisites: res
                })
            })
        }
    }

    // submit course form
    submitForm = (values) => {
        // check if editing, or creating a new course
        const methodName = this.state.edit ? 'editCourse' : 'createCourse';
        const courseId = this.props.match.params.id;

        values.price = parseFloat(values.price);

        if (this.state.edit) {
            values.courseId = courseId;
        }

        // do server call
        Meteor.call(methodName, values, (err) => {
            if (!err) {
                if (this.state.edit) {
                    // if edit, go to course view
                    this.goToCourseView();
                } else {
                    // else, go to all courses view
                    this.props.history.push('/my-courses');
                }
            }
        });
    }

    // remove all uploaded files, of a lesson
    removeUploadedFilesOfVideo = (video) => {
        // if a lesson has a video url, delete it
        if (video.url) {
            Meteor.call('deleteVideoFromStorage', video.url);
        }

        // if a lesson has a thumbnail, delete it
        if (video.thumbnail) {
            Meteor.call('deleteThumbnailFromStorage', video.thumbnail);
        }

        // if a lesson has an attachment, delete it
        if (video.attachment) {
            Meteor.call('deleteAttachmentFromStorage', video.attachment);
        }
    }

    // redirects to course view page
    goToCourseView = () => {
        this.props.history.push(`/my-courses/view/${this.props.match.params.id}`);
    }

    render() {
        const { edit, initialValues, isLoading } = this.state;
        if (isLoading) {
            return <div> Loading ...</div>;
        }

        return (
            <div>
                <div className="columns">
                    <div className="column is-four-fiths">
                        <h2 className="pb1">{edit ? 'Edit Course' : 'Create Course'}</h2>
                    </div>
                    {
                        edit &&
                        <div className="column">
                            <button
                                className="button is-link is-pulled-right"
                                type="button"
                                onClick={this.goToCourseView}
                            >
                                VIEW
                        </button>
                        </div>
                    }
                </div>

                <Formik
                    initialValues={initialValues}
                    validationSchema={CourseSchema}
                    onSubmit={(values, actions) => {
                        this.submitForm(values);
                        actions.setSubmitting(false);
                    }}
                >
                    {({ handleSubmit, values, errors, touched }) => {
                        console.log('errors:', errors)
                        return (
                            <form className="formik-form" onSubmit={handleSubmit}>
                                <div className="columns">
                                    <div className="column">
                                        <Input
                                            name="name"
                                            label="Name*"
                                            placeholder="Name"
                                        />

                                        <Input
                                            name="description"
                                            label="Description*"
                                            placeholder="Description"
                                        />

                                        <Input
                                            name="price"
                                            label="Price*"
                                            placeholder="Price"
                                            type="number"
                                        />

                                        <FileInput
                                            className='mb2'
                                            name='courseThumbnail'
                                            label='Course Thumbnail* (png/jpeg)'
                                            uploadTarget="aws-s3-thumbnailsUpload"
                                        />



                                        {
                                            this.state.availablePrerequisites.length
                                                ?
                                                <SearchableSelect
                                                    label={"Prerequisites"}
                                                    options={this.state.availablePrerequisites}
                                                    name="prerequisites"
                                                    multiple={true}
                                                    placeholder="Prerequisites"
                                                />
                                                :
                                                <div></div>
                                        }
                                    </div>
                                    <div className="column">
                                        <Select
                                            name="category"
                                            placeholder="Category"
                                            label="Category*"
                                            options={CATEGORIES}
                                        />

                                        <Select
                                            name="level"
                                            placeholder="Level"
                                            label="Level of difficulty*"
                                            options={LEVELS}
                                        />

                                        <DatePicker
                                            name="examDate"
                                            placeholder="Click to select a date"
                                            label="Exam Date"
                                        />

                                        <Input
                                            name="examRoom"
                                            label="Exam Room"
                                            placeholder="Please type the room's name"
                                        />
                                    </div>
                                </div>

                                <FieldArray
                                    name="videos"
                                    render={arrayHelpers => (
                                        <div style={{ marginVertical: '15px' }}>
                                            {values.videos && values.videos.map((videoFile, index) => (
                                                <div className="box" key={index}>
                                                    <div className="mb1">
                                                        <strong className="has-text-link">{`Lesson ${index + 1}`}</strong>
                                                    </div>

                                                    <Input
                                                        name={`videos.${index}.name`}
                                                        label="Name*"
                                                        placeholder='Name'
                                                    />
                                                    <Input
                                                        name={`videos.${index}.description`}
                                                        label="Description"
                                                        placeholder='Description'
                                                    />

                                                    <Checkbox
                                                        name={`videos.${index}.isLocked`}
                                                        label="Is Locked*"
                                                    />

                                                    <FileInput
                                                        className='mb1'
                                                        name={`videos.${index}.url`}
                                                        label='Lesson Video*'
                                                        index={index}
                                                        uploadTarget="aws-s3-videosUpload"
                                                    />

                                                    <FileInput
                                                        className='mb1'
                                                        name={`videos.${index}.thumbnail`}
                                                        label='Lesson thumbnail'
                                                        index={index}
                                                        uploadTarget="aws-s3-thumbnailsUpload"
                                                    />

                                                    <FileInput
                                                        className='mb1'
                                                        name={`videos.${index}.attachment`}
                                                        label='Lesson attachment'
                                                        index={index}
                                                        uploadTarget="aws-s3-attachmentsUpload"
                                                    />

                                                    <button
                                                        className="button is-danger"
                                                        type="button"
                                                        onClick={() => {
                                                            arrayHelpers.remove(index);
                                                            this.removeUploadedFilesOfVideo(videoFile);
                                                        }}
                                                    >
                                                        Remove Lesson
                                                    </button>
                                                </div>
                                            ))
                                            }
                                            <ErrorMessage name='videos' render={msg => {
                                                const isString = typeof (msg) === 'string';
                                                return isString && <div className="error-field">{msg}</div>
                                            }} />

                                            <button
                                                type="button"
                                                className='button is-link mt1'
                                                onClick={() => {
                                                    const index = values.videos ? values.videos.length : 0
                                                    if (index) {
                                                        arrayHelpers.insert(index, {
                                                            name: '',
                                                            description: '',
                                                            url: '',
                                                            attachment: '',
                                                            isLocked: false
                                                        });
                                                    } else {
                                                        arrayHelpers.push({
                                                            name: '',
                                                            description: '',
                                                            url: '',
                                                            attachment: '',
                                                            isLocked: false
                                                        })
                                                    }
                                                }}>
                                                Add Lesson
                                            </button>

                                        </div>
                                    )} />

                                <FieldArray
                                    name="tags"
                                    render={arrayHelpers => (
                                        <div className="mt2" style={{ marginVertical: '15px' }}>
                                            <div className="mt1 mb1">
                                                <strong>Tags: </strong>
                                            </div>
                                            <div className="columns fw-w">
                                                {values.tags && values.tags.map((tag, index) => (
                                                    <div className="column is-one-fifth" key={index}>
                                                        <Input
                                                            className="course-tag"
                                                            name={`tags.${index}`}
                                                            placeholder='Tag here...'
                                                            onClickButton={() => {
                                                                arrayHelpers.remove(index);
                                                            }}
                                                        />
                                                    </div>
                                                ))
                                                }
                                            </div>

                                            <ErrorMessage name='tags' render={msg => {
                                                const isString = typeof (msg) === 'string';
                                                return isString && <div className="error-field">{msg}</div>
                                            }} />

                                            <button
                                                type="button"
                                                className='button is-link mb1 mt1'
                                                onClick={() => {
                                                    const index = values.tags ? values.tags.length : 0
                                                    if (index) {
                                                        arrayHelpers.insert(index, '');
                                                    } else {
                                                        arrayHelpers.push('')
                                                    }
                                                }}>
                                                Add Tag
                                            </button>

                                        </div>
                                    )} />
                                <button type="submit" className="button is-primary mt2">
                                    {this.state.edit ? 'Save' : 'Create'}
                                </button>
                            </form>
                        )
                    }}

                </Formik>
            </div>
        );
    }
}
