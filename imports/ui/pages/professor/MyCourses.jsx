import React, { Component } from 'react';
import { LinkButton } from '/imports/ui/components/elements';
import { Meteor } from 'meteor/meteor';
import CourseSingle from './CourseSingle.jsx';

export default class MyCourses extends Component {
    // initial state of the component
    state = {
        courses: []
    }

    // get courses for the currently logged in component
    componentDidMount() {
        // api call, getting the courses from the server
        // the first parameter is null, so the method gets the id of the currently logged in user
        Meteor.call('getCoursesForProfessor', null, (err, res) => {
            if (!err) {
                this.setState({
                    courses: res
                })
            }
        });
    }

    render() {
        const { courses } = this.state;

        return (
            <div>
                <h1 className="title mb2">My courses</h1>
                <div className="columns fw-w">
                    {
                        courses.map(course => <CourseSingle key={course._id} course={course} />)
                    }
                </div>
                <LinkButton to={"/my-courses/create"}>
                    Create a new Course!
                </LinkButton>
            </div>
        );
    }
}