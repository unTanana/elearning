import * as Yup from 'yup';

// course object validation schema
export default Yup.object().shape({
    name: Yup.string()
        .required('Required'),
    description: Yup.string()
        .required('Required'),
    category: Yup.string()
        .required('Required'),
    level: Yup.string()
        .required('Required'),
    courseThumbnail: Yup.string()
        .required('Required'),
    prerequisites: Yup.array().of(Yup.string().required('Required')),
    price: Yup.number().required('Required').min(0, 'Cannot be lower than 0!'),
    videos: Yup.array().of(Yup.object().shape({
        name: Yup.string().required('Required'),
        description: Yup.string(),
        url: Yup.string().required('Uploading a video is required'),
        thumbnail: Yup.string(),
        attachment: Yup.string(),
        isLocked: Yup.boolean().required('Required!')
    }).required('Required')).min(1, 'At least 1 video is required'),
    tags: Yup.array().of(Yup.string().required('Required')),
    examDate: Yup.date(),
    examRoom: Yup.string()
});