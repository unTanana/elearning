import React from 'react';
import { VideoPlayer } from '/imports/ui/components/elements';

export default class CourseProfessorView extends React.Component {
    // initial state of the app
    state = {
        course: null
    }

    // get course by id, on component mount
    componentDidMount() {
        // api call to the server, that gets the course data
        Meteor.call('getCourseById', this.props.match.params.id, (err, res) => {
            if (!err) {
                this.setState({
                    course: res
                });
            }
        });
    }

    // redirects to course edit view
    goToCourseEditView = () => {
        this.props.history.push(`/my-courses/edit/${this.props.match.params.id}`);
    }

    // deletes course, and redirects to all courses page
    deleteCourse = () => {
        if (confirm('Are you sure you want to delete this course?')) {
            Meteor.call('deleteCourse', this.props.match.params.id, () => {
                this.props.history.push('/my-courses');
            });
        }
    }

    render() {
        const { course } = this.state;

        if (!course) {
            return <div> Loading ...</div>;
        }

        return (
            <div className="course-view container">
                <section className="columns pb2">
                    <div className="column is-four-fifths">
                        <h1 className="title">
                            {course.name}
                        </h1>
                    </div>
                    <div className="column">
                        <button
                            className="button is-link is-pulled-right"
                            type="button"
                            onClick={this.goToCourseEditView}
                        >
                            EDIT
                        </button>

                        <button
                            className="button is-danger is-pulled-right mr1"
                            type="button"
                            onClick={this.deleteCourse}
                        >
                            DELETE
                        </button>
                    </div>
                </section>

                <div className="columns pb2">
                    <div className="column is-half">
                        <figure className="image">
                            <img src={course.courseThumbnail} alt="Course Thumbnail" />
                        </figure>
                    </div>
                    <div className="column is-half">
                        <div className="subtitle">
                            {course.description}
                        </div>
                    </div>
                </div>

                <div className="columns box pb2">
                    <div className="column is-half">
                        <h1 className="title">Category:</h1>
                        <div className="subtitle pb2">{course.category}</div>
                        <h1 className="title">Difficulty:</h1>
                        <div className="subtitle">{course.level}</div>
                    </div>
                    <div className="column is-half">
                        <h1 className="title">Price:</h1>
                        <div className="className pb2">
                            {course.price}
                        </div>
                        {
                            course.tags && course.tags.length
                                ?
                                <>
                                    <h1 className="title">Tags:</h1>
                                    {
                                        course.tags.map(tag => (
                                            <span key={tag} className="tag is-primary mr1">{tag}</span>
                                        ))
                                    }
                                </>
                                :
                                <></>
                        }
                    </div>
                </div>

                <h1 className="title pt1">Lessons:</h1>
                <div className="columns fw-w">
                    {
                        course.videos && course.videos.map(video => (
                            <div key={video.url} className="column is-half video">
                                <VideoPlayer
                                    src={video.url}
                                    thumbnail={video.thumbnail}
                                />
                                <div className="subtitle mt1">
                                    {video.name}
                                </div>
                                <div className="description">
                                    {video.description}
                                </div>
                                {
                                    video.attachment &&
                                    <div className="attachment mt2">
                                        <a
                                            target="_blank"
                                            className="button is-primary"
                                            href={video.attachment}
                                            download="attachment">
                                            See attachment
                                        </a>
                                    </div>
                                }
                            </div>
                        ))
                    }
                </div>
            </div>
        )
    }
}