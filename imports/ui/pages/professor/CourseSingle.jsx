import React from 'react';
import { withRouter } from 'react-router-dom';

// wrap Course Item in a withRouter component
// now the Course Item component has access to the history prop
// this allows the component to do redirects
export default withRouter(
    ({ course, history }) => {
        if (!course) {
            return <div>Loading ...</div>;
        }

        // redirect to view page, using the history component
        const viewCourse = () => {
            history.push(`/my-courses/view/${course._id}`);
        }

        // shorten name of course, if it's too long
        const name = course.name.length < 20 ? course.name : course.name.slice(0, 17) + '...';

        return (
            <div className="course-presentation-item column is-one-third">
                <div className="card">
                    <div className="card-image" onClick={viewCourse}>
                        <figure className="image">
                            <img className="max-h-105" src={course.courseThumbnail} alt="Course Thumbnail" />
                        </figure>
                    </div>
                    <div className="card-content">
                        <div className="media">
                            <div className="media-content">
                                <p className="title is-4">{name}</p>
                            </div>
                        </div>

                        <div className="content mb2">
                            <div className="level is-pulled-left has-text-primary">
                                {course.level}
                            </div>
                            <div className="price is-pulled-right">
                                {course.price}$
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
)