import React, { Component } from 'react';
import LibraryItem from './components/StudentLibraryItem.jsx';

export default class StudentLibrary extends Component {
    // initial state of the component
    state = {
        loading: true,
        courses: null,
        tags: null
    }

    // gets all courses of student, on mount
    componentDidMount() {
        this.getCourses();
    }

    // gets all courses of a student
    getCourses = () => {
        // api call to get all courses of a student, from the server
        // the first parameter is null, so the courses of the currently logged in user are fetched
        Meteor.call('getStudentCourses', null, (err, res) => {
            if (!err) {
                // set courses in the state
                this.setState({
                    courses: res,
                    loading: false
                });
            }
        })
    }

    // redirects to /
    goToMainScreen = () => {
        this.props.history.push('/')
    }

    render() {
        if (this.state.loading) {
            return <div>Loading ...</div>
        }

        const { courses } = this.state;

        return (
            <div className="lessons-page">
                <div className="columns mt1 fw-w">
                    <div className="column is-three-quarters">
                        {
                            !courses || !courses.length &&
                            <div>
                                <p className="title mb2">
                                    You have no courses in your library ...
                                    </p>
                                <button
                                    className="button is-primary"
                                    type="button"
                                    onClick={this.goToMainScreen}
                                >
                                    See All Courses
                                </button>
                            </div>

                        }
                        {
                            courses.map(course => (
                                <LibraryItem key={course._id} course={course} />
                            ))
                        }
                    </div>
                    <div className="column is-one-quarter">
                        {
                            courses && courses.length ?
                                <button
                                    type="button"
                                    onClick={() => { this.props.history.push('/') }}
                                    className="button is-link">
                                    Back
                                    </button>
                                :
                                <div></div>
                        }
                    </div>
                </div>
            </div>
        );
    }
}

