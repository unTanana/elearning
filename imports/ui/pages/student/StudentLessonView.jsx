import React from 'react';
import ActiveLesson from "./components/ActiveLesson.jsx";
import CourseContents from './components/CourseContents.jsx';
import LessonDetails from './components/LessonDetails';
import { connect } from 'react-redux';

// student lesson view, connected to the store, to see progress, and update it
export default connect(
    state => ({
        courseProgress: state.course.selectedCourseProgress
    }),
    dispatch => ({
        updateCourseProgress: dispatch.course.updateSelectedCourseProgress
    })
)(class StudentLessonView extends React.Component {
    // initial state of the component
    state = {
        course: null,
        activeLesson: 0,
        activeTab: 'contents'
    }

    // on mount, get the course
    componentDidMount() {
        this.getCourse();
    }

    // gets course by it's id
    getCourse() {
        // get id
        const id = this.props.match.params.id;
        // api call, to get the course by it's id
        Meteor.call('getCourseById', id, (err, res) => {
            if (!err) {
                this.setState({
                    course: res
                });
                // set course progress in the store
                this.props.updateCourseProgress(res.progress || 0);
            }
        });
    }

    // helper to get tab active class
    getTabClass = (tab) => {
        return this.state.activeTab === tab ? 'is-active' : ''
    }

    // helper to set active tab
    setActiveTab = (tab) => {
        this.setState({
            activeTab: tab
        });
    }

    // sets index of active lesson
    onLessonClick = (index) => {
        this.setState({
            activeLesson: index
        });
    }

    render() {
        const { course, activeLesson } = this.state;

        if (!course) {
            return <div> Loading ...</div>;
        }

        return (
            <div className="student-lesson-view">
                <div className="subtitle">Course Progress:</div>
                <progress className="progress is-small is-primary" value={this.props.courseProgress} max="100"></progress>

                <div className="title">
                    {course.name}
                </div>

                <div className="columns">
                    <div className="column is-three-fifths">
                        <ActiveLesson
                            courseId={course._id}
                            lessonIndex={activeLesson}
                            lesson={course.videos[activeLesson]}
                        />
                    </div>
                    <div className="column is-two-fifths">
                        <div className="tabs mt2 is-toggle is-medium">
                            <ul>
                                <li className={this.getTabClass('contents')}>
                                    <a onClick={() => this.setActiveTab('contents')}>
                                        Course Contents
                            </a>
                                </li>
                                <li className={this.getTabClass('details')}>
                                    <a onClick={() => this.setActiveTab('details')}>
                                        Lesson Details
                            </a>
                                </li>
                            </ul>
                        </div>
                        {
                            this.state.activeTab === 'contents' ?
                                <CourseContents
                                    course={course}
                                    onLessonClick={this.onLessonClick}
                                    activeLesson={this.state.activeLesson}
                                />
                                :
                                <LessonDetails
                                    course={course}
                                    activeLesson={this.state.activeLesson}
                                />
                        }
                    </div>
                </div>
            </div>
        )
    }
})