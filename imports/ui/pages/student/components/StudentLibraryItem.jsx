import React from 'react';
import { withRouter } from 'react-router-dom';

// Student Library CourseItem Component
// wrapped in a withRouter component
// this gives it access to the history prop, allowing redirects
export default withRouter(
    ({ course, history }) => {
        if (!course) {
            return <div>Loading ...</div>;
        }

        // redirect to course view component using the history prop
        const viewCourse = () => {
            history.push(`/library/view/${course._id}`);
        }

        return (
            <div className="library-item box">
                <article className="media">
                    <div className="media-left">
                        <figure className="image is-128x128">
                            <img src={course.courseThumbnail} alt="Image" />
                        </figure>
                    </div>
                    <div className="media-content">
                        <div className="content">
                            <p>
                                <strong>{course.name}</strong>
                                <br />
                                {course.description}
                            </p>
                        </div>
                        <nav className="level">
                            <div className="level-left">
                                <small>
                                    Author: {course.professor.emails[0].address}
                                </small>
                            </div>
                            <div className="level-right">
                                <button
                                    type="button"
                                    onClick={viewCourse}
                                    className="button is-link">
                                    Go to Course
                                </button>
                            </div>
                        </nav>
                    </div>
                </article>
            </div>
        );
    }
)