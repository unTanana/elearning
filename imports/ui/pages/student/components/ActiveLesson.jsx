import React from 'react';
import { VideoPlayer } from '/imports/ui/components/elements';
import { debounce } from 'lodash';
import { Meteor } from 'meteor/meteor';
import { dispatch } from '/imports/store/store.js';

export default class ActiveLesson extends React.Component {
    constructor(props) {
        super(props);

        // initial state of the component
        this.state = {
            progress: 0,
            lessonIndex: props.lessonIndex
        }
    }

    // handles video state change
    onVideoStateChange = (state) => {
        // calculate progress
        const progress = state.currentTime / state.duration * 100;

        // if progress has changed with at least 4, save it in state, and db
        if (progress >= this.state.progress + 4) {
            this.setState({
                progress
            }, this.saveProgress);
        }
    }

    // save progress of course in db
    saveProgress = () => {
        const { courseId, lessonIndex } = this.props;
        const { progress } = this.state;

        // api call that saves the lesson progress
        Meteor.call('updateLessonProgress', null, courseId, lessonIndex, progress, (err, res) => {
            // if no error, and the lesson is completed (res is true)
            // update the store with the new global progress on the current course
            if (!err && res) {
                dispatch.course.updateSelectedCourseProgress(res);
            }
        })
    }

    // on lesson change, reset the state
    static getDerivedStateFromProps(nextProps, prevState) {
        // if the lesson index has changed, reset the state
        if (nextProps.lessonIndex !== prevState.lessonIndex) {
            return {
                progress: 0,
                lessonIndex: nextProps.lessonIndex
            }

        }

        return null;
    }

    // on lesson change, reset the video component
    componentDidUpdate(prevProps, prevState) {
        // if the lesson index has changed
        if (this.state.lessonIndex !== prevState.lessonIndex) {
            // reload the video component
            this.refs.playerWrapper.refs.player.load();
        }
    }

    render() {
        const { lesson } = this.props;

        return (
            <div className="active-lesson">
                <div className="mt2">
                    <VideoPlayer
                        ref="playerWrapper"
                        src={lesson.url}
                        thumbnail={lesson.thumbnail}
                        onStateChange={debounce(this.onVideoStateChange, 250)}
                    />
                </div>

            </div>
        );
    }

}