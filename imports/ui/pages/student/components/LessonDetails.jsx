import React from 'react';

// component that displays the details of the active lesson
export default ({ course, activeLesson }) => {
    // get active lesson
    const lesson = course.videos[activeLesson];

    return (
        <div className="course-contents box">
            {
                lesson.attachment
                    ?
                    <div className="attachment">
                        <a
                            target="_blank"
                            className="button is-primary"
                            href={lesson.attachment}
                            download="attachment">
                            See attachment
                        </a>
                    </div>
                    :
                    <p>No Attachments</p>
            }
        </div>
    )
}