import React from 'react';

// component that displays all available lessons of a course
export default ({ course, activeLesson, onLessonClick }) => {

    return (
        <div className="course-contents box">
            {
                course.videos.map((video, index) => (
                    <article
                        key={index}
                        className={`message ${activeLesson === index ? 'is-primary' : ''}`}
                        onClick={() => onLessonClick(index)}
                    >
                        <div className="message-header">
                            <p>{index + 1}. {video.name} {video.isCompleted ? ' (Completed)' : ''}</p>
                        </div>
                        <div className="message-body">
                            {video.description}
                        </div>
                    </article>
                ))
            }
        </div>
    )
}