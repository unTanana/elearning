import React, { Component } from 'react';
import { Formik } from 'formik';
import { Input } from '/imports/ui/components/formik';
import * as Yup from 'yup';

// schema for the password setting form
// passwords must match, and are required
const RegisterSchema = Yup.object().shape({
    password1: Yup.string()
        .required('Required'),
    password2: Yup.string()
        .oneOf([Yup.ref('password1'), null], 'Passwords do not match!')
        .required('Required'),
});

class PasswordForm extends Component {
    // handles form submission
    submitForm = ({ password1 }) => {
        if (this.props.onPasswordSubmit) {
            this.props.onPasswordSubmit(password1);
        }
    };

    render() {
        return (
            <div>
                <h1>
                    Set up a password
                </h1>
                <Formik
                    initialValues={{
                        password1: "",
                        password2: ""
                    }}
                    validationSchema={RegisterSchema}
                    onSubmit={(values, actions) => {
                        this.submitForm(values);
                        actions.setSubmitting(false);
                    }}
                >
                    {({ handleSubmit }) => {
                        return (
                            <form className="formik-form" onSubmit={handleSubmit}>

                                <Input
                                    name="password1"
                                    placeholder="Password"
                                    type="password"
                                />

                                <Input
                                    name="password2"
                                    placeholder="Confirm password"
                                    type="password"
                                />

                                <button type="submit" className="button is-primary">Register</button>
                            </form>)
                    }}

                </Formik>
            </div>
        );
    }

}

export default PasswordForm;
