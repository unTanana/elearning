import React, { Component } from 'react';
import { Formik } from 'formik';
import { Input } from '/imports/ui/components/formik';
import * as Yup from 'yup';

// schema of the registration form
// email is required, and must look like an email
const RegisterSchema = Yup.object().shape({
    email: Yup.string()
        .email('Invalid email')
        .required('Required'),
});

class EmailForm extends Component {
    // handles email submission
    submitForm = ({ email }) => {
        if (this.props.onEmailSubmit) {
            this.props.onEmailSubmit(email);
        }
    };

    render() {
        return (
            <div>
                <h1>
                    Enter your email
                </h1>
                <Formik
                    initialValues={{
                        email: "",
                    }}
                    validationSchema={RegisterSchema}
                    onSubmit={(values, actions) => {
                        this.submitForm(values);
                        actions.setSubmitting(false);
                    }}
                >
                    {({ handleSubmit }) => {
                        return (
                            <form className="formik-form" onSubmit={handleSubmit}>

                                <Input
                                    name="email"
                                    placeholder="email"
                                    type="email"
                                />

                                <button type="submit" className="button is-primary">Next</button>
                            </form>)
                    }}

                </Formik>
            </div>
        );
    }

}

export default EmailForm;
