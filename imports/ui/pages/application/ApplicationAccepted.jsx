import React, { Component } from 'react';
import EmailForm from './components/EmailForm.jsx';
import PasswordForm from './components/PasswordForm.jsx';

class ApplicationAccepted extends Component {
    // initial state of the component
    state = {
        showPassword: false,
        loading: false,
        error: null,
        userCreated: false
    };

    // gets applicationId based on email
    onEmailSubmit = (email) => {
        this.setState({ loading: true });
        Meteor.call('checkApplicationEmail', { email }, (err, { success, error, applicationId }) => {
            if (err) {
                this.setState({ error: 'Internal server error' });
            } else {
                if (success) {
                    this.applicationId = applicationId;

                    this.setState({ showPassword: true });
                } else {
                    this.setState({ error });
                }
            }
            this.setState({ loading: false });
        });
    };

    // handles registration of user, after password is set
    onPasswordSubmit = (password) => {
        const { history } = this.props;

        this.setState({ loading: true });
        const applicationId = this.applicationId;

        // call that creates the user
        Meteor.call('registerApplicationUser', { password, applicationId }, (err, userId) => {
            if (err) {
                this.setState({ error: 'Internal server error' });
            } else {
                this.setState({ userCreated: true });

                setTimeout(() => {
                    history.push('/login');
                }, 2000);
            }
            this.setState({ loading: false });
        });
    };

    render() {
        const { showPassword, loading, error, userCreated } = this.state;

        return (
            <div>
                {
                    loading ?
                        <div>
                            Loading...
                        </div>
                        :
                        <div>
                            {
                                userCreated ?
                                    <div className="mt2">
                                        <span>Your account has been created. You will shortly be redirected to the login page.</span>
                                    </div>
                                    :
                                    <div>
                                        {
                                            !showPassword &&
                                            <EmailForm onEmailSubmit={this.onEmailSubmit} />
                                        }
                                        {
                                            showPassword &&
                                            <PasswordForm onPasswordSubmit={this.onPasswordSubmit} />
                                        }
                                    </div>
                            }
                        </div>
                }
                {
                    error ?
                        <div className="mt2">
                            <span>{error}</span>
                        </div>
                        :
                        null
                }
            </div>
        );
    }

}

export default ApplicationAccepted;
