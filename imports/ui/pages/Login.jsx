import React, { PureComponent } from 'react'
import { Formik, ErrorMessage } from 'formik';
import { Input } from '/imports/ui/components/formik';
import { Meteor } from 'meteor/meteor'
import * as Yup from 'yup';

// Login form schema
const LoginSchema = Yup.object().shape({
    email: Yup.string()
        .email('Invalid email')
        .required('Required'),
    password: Yup.string()
        .required('Required')
});

export default class Login extends PureComponent {

    // form submit function, logging in the student
    submitForm = (data) => {
        // Using the Meteor built in function, log in the user
        Meteor.loginWithPassword(data.email, data.password, (err) => {
            if (!err) {
                // if no error occurred,
                this.props.history.push('/');
            }
        });
    }

    render() {
        return (
            <div>
                <h1 className="title">
                    Log In
                </h1>
                <Formik
                    initialValues={{
                        email: '',
                        password: ''
                    }}
                    validationSchema={LoginSchema}
                    onSubmit={(values, actions) => {
                        this.submitForm(values);
                        actions.setSubmitting(false);
                    }}
                >
                    {({ handleSubmit }) => {
                        return (
                            <form className="formik-form" onSubmit={handleSubmit}>
                                <Input
                                    name="email"
                                    placeholder="email"
                                    type="email"
                                />
                                <Input
                                    name="password"
                                    placeholder="Password"
                                    type="password"
                                />
                                <button className="button is-primary" type="submit">Login</button>
                            </form>)
                    }}

                </Formik>
            </div>
        )
    }
}
