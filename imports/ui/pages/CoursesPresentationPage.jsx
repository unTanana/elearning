import React, { Component } from 'react';
import CoursePresentationItem from './common/CourseItem.jsx';
import { connect } from 'react-redux';
import CourseFilters from './../components/common/CourseFilters.jsx';

// connects component to the store, giving access to the filters, and filters menu state
export default connect(
    state => ({
        course: state.course,
    }),
    dispatch => ({
        // helper to toggle the filters menu
        toggleFiltersMenu: dispatch.course.toggleFiltersMenu,
    })
)(class CoursesPresentationPage extends Component {
    // initial state of the component
    state = {
        loading: true,
        courses: null,
        tags: null
    }

    // on mount, get all courses using the filters from the store
    componentDidMount() {
        this.getFilteredCourses();
    }

    // get all courses, using the filters from the store
    getFilteredCourses = () => {
        // get filters from the store
        const filters = this.props.course;

        // api call, to get filtered courses from the server
        Meteor.call('getCoursesData', filters, (err, res) => {
            if (!err) {
                // save courses and tags into the state
                this.setState({
                    courses: res[0],
                    tags: res[1],
                    loading: false
                });
            }
        })
    }

    // toggles filters menu
    turnOffMenu = () => {
        if (this.props.course.isFiltersMenuVisible) {
            this.props.toggleFiltersMenu();
        }
    }

    render() {
        if (this.state.loading) {
            return <div>Loading ...</div>
        }

        const { courses } = this.state;

        return (
            <div className="main-page" onClick={this.turnOffMenu}>
                <CourseFilters onFilterChange={this.getFilteredCourses} filterCourses={this.getFilteredCourses}/>
                <div className="columns mt1 fw-w">
                    {
                        courses.map(course => (
                            <CoursePresentationItem key={course._id} course={course}/>
                        ))
                    }
                </div>
            </div>
        );
    }
})
