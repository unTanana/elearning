import React, { Component } from 'react';
import ApplicationManager from './sub-pages/ApplicationManager.jsx';

// Dashboard component, contains the Applications Manager
class Dashboard extends Component {
    render() {

        return (
            <div>
                <h2>Admin Dashboard!</h2>
                <ApplicationManager />
            </div>
        );
    }

}

export default Dashboard;
