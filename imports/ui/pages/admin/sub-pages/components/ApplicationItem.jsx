import React from 'react';
import { Meteor } from 'meteor/meteor';

export default ({ application, onAction }) => {
    // handles application approval
    // asks for confirmation
    const approveApplication = () => {
        if (confirm('Are you sure you want to APPROVE this application?')) {
            Meteor.call('approveApplication', application._id, (err) => {
                if (!err) {
                    onAction();
                }
            });
        }
    }

    // handles application rejection
    // asks for confirmation
    const rejectApplication = () => {
        if (confirm('Are you sure you want to REJECT this application?')) {
            Meteor.call('rejectApplication', application._id, (err) => {
                if (!err) {
                    onAction();
                }
            });
        }
    }


    return (
        <div className="application">
            <div className="card">
                <header className="card-header">
                    <p className="card-header-title">
                        {application.name}
                    </p>
                </header>
                <div className="card-content">
                    <div className="content">
                        <bold>
                            {application.email}
                        </bold>
                        <p>
                            {application.about}
                        </p>
                    </div>
                </div>
                {
                    onAction &&
                    <footer className="card-footer">
                        <a
                            onClick={approveApplication}
                            className="has-text-primary card-footer-item"
                        >
                            Approve
                    </a>
                        <a
                            onClick={rejectApplication}
                            className="has-text-danger card-footer-item"
                        >
                            Reject
                    </a>
                    </footer>
                }

            </div>
        </div>
    );
}