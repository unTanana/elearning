import React from 'react';
import { Meteor } from 'meteor/meteor';
import ApplicationItem from './components/ApplicationItem.jsx';

export default class ApplicationManager extends React.Component {
    // definie initial filters and an empty list of applications  
    state = {
        status: 'pending',
        type: 'student',
        applications: []
    };

    // fetch all applications on mount
    componentDidMount() {
        this.getApplications();
    }

    // get filters from state, and fetch applications
    getApplications = () => {
        const filters = {
            status: this.state.status,
            type: this.state.type
        }

        // the actual call that gets the applications
        Meteor.call('getApplications', filters, (err, res) => {
            if (!err) {
                this.setState({
                    applications: res
                })
            }
        })
    }

    // helper function that checks if a type tab is active
    isActiveType = (type) => {
        return type === this.state.type;
    }

    // helper function that checks if a status tab is active
    isActiveStatus = (status) => {
        return status === this.state.status;
    }

    // handles type filter change
    changeType = (type) => {
        if (type !== this.state.type) {
            this.setState({
                type
            }, this.getApplications);
        }
    }

    // handles status filter change
    changeStatus = (status) => {
        if (status !== this.state.status) {
            this.setState({
                status
            }, this.getApplications);
        }
    }

    render() {
        const { applications } = this.state;

        return (
            <div className="application-manager">
                <div className="columns">
                    <div className="column">
                        <div className="tabs is-toggle is-toggle-rounded is-left">
                            <ul>
                                <li className={this.isActiveType('student') ? 'is-active' : ''}>
                                    <a onClick={this.changeType.bind(this, 'student')}>
                                        <span className="icon is-small"><i className="fas fa-graduation-cap"></i></span>
                                        <span>Student</span>
                                    </a>
                                </li>
                                <li className={this.isActiveType('professor') ? 'is-active' : ''}>
                                    <a onClick={this.changeType.bind(this, 'professor')}>
                                        <span className="icon is-small"><i className="fas fa-university"></i></span>
                                        <span>Professor</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="column">
                        <div className="tabs is-toggle is-toggle-rounded is-right">
                            <ul>
                                <li className={this.isActiveStatus('pending') ? 'is-active' : ''}>
                                    <a onClick={this.changeStatus.bind(this, 'pending')}>
                                        <span className="icon is-small"><i className="fas fa-tasks"></i></span>
                                        <span>Pending</span>
                                    </a>
                                </li>
                                <li className={this.isActiveStatus('approved') ? 'is-active' : ''}>
                                    <a onClick={this.changeStatus.bind(this, 'approved')}>
                                        <span className="icon is-small"><i className="fas fa-thumbs-up"></i></span>
                                        <span>Approved</span>
                                    </a>
                                </li>
                                <li className={this.isActiveStatus('rejected') ? 'is-active' : ''}>
                                    <a onClick={this.changeStatus.bind(this, 'rejected')}>
                                        <span className="icon is-small"><i className="fas fa-thumbs-down"></i></span>
                                        <span>Rejected</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div className="applications-list">
                    {
                        applications.map(application => (
                            <ApplicationItem
                                key={application._id}
                                application={application}
                                onAction={
                                    this.state.status === 'pending' ? this.getApplications : undefined
                                }
                            />
                        ))
                    }
                </div>
            </div>
        )
    }
}