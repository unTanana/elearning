import React, { Component } from 'react';
import { Formik } from 'formik';
import { Input, Select } from '/imports/ui/components/formik';
import * as Yup from 'yup';
import { Meteor } from 'meteor/meteor';

// schema of the application form
// all fields are required
// email must look like an email
const formSchema = Yup.object().shape({
    name: Yup.string()
        .required('Required'),
    email: Yup.string()
        .email('Invalid email')
        .required('Required'),
    about: Yup.string()
        .required('Required'),
    type: Yup.string()
        .required('Required')
});

// define type of applications
const applicationTypes = [
    {
        value: 'student',
        title: 'Student',
    },
    {
        value: 'professor',
        title: 'Professor',
    },
];

export default class ApplicationForm extends Component {
    // initial state of the component 
    state = {
        initialValues: {
            name: '',
            email: '',
            about: '',
            type: 'student',
        }
    }

    // handles form submission
    // registers a new application
    submitForm = (values) => {
        // call to register the application
        Meteor.call('registerApplication', values, (err, res) => {
            if (!err) {
                alert('Application submitted successfully!');
            } else {
                alert('An error has occurred...');
            }

            this.props.history.push('/');
        })
    }

    render() {
        const { initialValues } = this.state;

        return (
            <div>
                <div className="title">
                    Application Form
                </div>

                <Formik
                    initialValues={initialValues}
                    validationSchema={formSchema}
                    onSubmit={(values, actions) => {
                        this.submitForm(values);
                        actions.setSubmitting(false);
                    }}
                >
                    {({ handleSubmit, values, errors, touched }) => {
                        console.log('errors:', errors)
                        return (
                            <form className="formik-form" onSubmit={handleSubmit}>
                                <Input
                                    name="name"
                                    label="Name"
                                    placeholder="Name"
                                />

                                <Input
                                    name="email"
                                    label="Email"
                                    placeholder="the@doe.com"
                                    type="email"
                                />

                                <Input
                                    name="about"
                                    label="About you:"
                                    placeholder="I am a nice person!"
                                />

                                <Select
                                    name="type"
                                    placeholder="student"
                                    label="What do you want to apply as?"
                                    options={applicationTypes}
                                />

                                <button type="submit" className="button is-primary mt1">
                                    Submit Application
                                </button>
                            </form>
                        )
                    }}

                </Formik>
            </div>
        );
    }
}
