import React from 'react';
import { Formik } from 'formik';
import { Input } from '/imports/ui/components/formik';
import * as Yup from 'yup';
import { Accounts } from 'meteor/accounts-base'

// validation schema for the profile form
// all fields except for the registrationNumber, are required
const ProfileSchema = Yup.object().shape({
    email: Yup.string()
        .email('Invalid email')
        .required('Required'),
    name: Yup.string().required('Required'),
    about: Yup.string().required('Required'),
    registrationNumber: Yup.string()
});

// validation schema for the password form
// all fields are required
const PasswordSchema = Yup.object().shape({
    oldPassword: Yup.string().required('Required'),
    newPassword: Yup.string().required('Required'),
});

export default class MyProfile extends React.Component {
    // initial state of the component
    state = {
        user: null,
        isLoading: true,
        isEdit: false,
        isChangingPassword: false
    }

    // get the active user on component mount
    componentDidMount() {
        Meteor.call('getActiveUser', (err, res) => {
            console.log('res:', res)
            if (!err) {
                this.setState({
                    user: res,
                    isLoading: false
                })
            }
        })
    }

    // toggles edit view of the profile form
    toggleEdit = () => {
        this.setState({
            isEdit: !this.state.isEdit
        });
    }

    // toggles change password form
    toggleChangingPassword = () => {
        this.setState({
            isChangingPassword: !this.state.isChangingPassword
        });
    }

    // submits profile form changes
    submitProfileForm = (values) => {
        if (confirm('Are you sure you want to make these changes?')) {
            // api call that does the actual changes
            Meteor.call('updateUserProfile', values, (err) => {
                if (!err) {
                    alert('Success!');
                } else {
                    alert('An error has occurred, please try again!');
                }

                // toggles form edit view
                this.toggleEdit();
            });
        };
    }

    // submits password form values
    submitPasswordsForm = (values) => {
        if (confirm('Are you sure you want to change your password?')) {
            // changes password of the user, using the built in Meteor Accounts package
            Accounts.changePassword(values.oldPassword, values.newPassword, (err) => {
                if (!err) {
                    alert('Success!');
                } else {
                    alert('An error has occurred, please try again!');
                }

                // toggles password form
                this.toggleChangingPassword();
            })
        }
    }

    render() {
        const { user, isLoading } = this.state;
        if (isLoading) {
            return <div> Loading ... </div>
        }

        return (
            <div className="my-profile mw-50 columns">
                <div className="column is-two-thirds">
                    <h1 className="title">
                        Your profile
                    </h1>
                    <Formik
                        initialValues={{
                            email: user.emails[0].address,
                            name: user.profile && user.profile.name,
                            about: user.profile && user.profile.about,
                            registrationNumber: user.registrationNumber
                        }}
                        validationSchema={ProfileSchema}
                        onSubmit={(values, actions) => {
                            this.submitProfileForm(values);
                            actions.setSubmitting(false);
                        }}
                    >
                        {({ handleSubmit, values, errors }) => {
                            const { isEdit } = this.state;
                            console.log('isEdit:', isEdit)

                            return (
                                <form className="formik-form" onSubmit={handleSubmit}>
                                    <Input
                                        name="email"
                                        placeholder="email"
                                        type="Your email"
                                        disabled={!isEdit}
                                        isSlow
                                        label="Email"
                                    />

                                    <Input
                                        name="name"
                                        placeholder="Your name"
                                        disabled={!isEdit}
                                        isSlow
                                        label="Name"
                                    />

                                    <Input
                                        name="about"
                                        placeholder="About you"
                                        disabled={!isEdit}
                                        isSlow
                                        label="About you"
                                    />

                                    {
                                        user && user.registrationNumber &&
                                        <Input
                                            name="registrationNumber"
                                            placeholder="Your registration number"
                                            disabled
                                            label="Registration Number"
                                        />
                                    }

                                    {
                                        isEdit
                                            ?
                                            <div className="flex">
                                                <button type="submit" className="button is-primary mr1">Save</button>
                                                <button
                                                    type="button"
                                                    className="button is-link"
                                                    onClick={this.toggleEdit}
                                                >
                                                    Cancel
                                                </button>
                                            </div>
                                            :
                                            <button
                                                type="button"
                                                className="button is-link"
                                                onClick={this.toggleEdit}
                                            >
                                                Edit
                                            </button>
                                    }
                                </form>)
                        }}

                    </Formik>
                </div>

                <div className="column is-one-third">
                    {
                        this.state.isChangingPassword
                            ?
                            <Formik
                                initialValues={{
                                    oldPassword: '',
                                    newPassword: ''
                                }}
                                validationSchema={PasswordSchema}
                                onSubmit={(values, actions) => {
                                    this.submitPasswordsForm(values);
                                    actions.setSubmitting(false);
                                }}
                            >
                                {({ handleSubmit, values, errors }) => {

                                    return (
                                        <form className="formik-form" onSubmit={handleSubmit}>
                                            <Input
                                                name="oldPassword"
                                                placeholder="Old Password"
                                                type="password"
                                                label="Old Password"
                                            />

                                            <Input
                                                name="newPassword"
                                                placeholder="New Password"
                                                type="password"
                                                label="New Password"
                                            />

                                            <div className="flex">
                                                <button type="submit" className="button is-primary mr1">Change</button>
                                                <button
                                                    type="button"
                                                    className="button is-link"
                                                    onClick={this.toggleChangingPassword}
                                                >
                                                    Cancel
                                                </button>
                                            </div>
                                        </form>)
                                }}

                            </Formik>
                            :
                            <button
                                type="button"
                                className="button is-link"
                                onClick={this.toggleChangingPassword}
                            >
                                Change Password
                            </button>
                    }
                </div>
            </div>
        )
    }
}