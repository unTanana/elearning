import React from 'react';
import { withRouter } from 'react-router-dom';

// wraps course item component in a withRouter wrapper
// this gives the component the ability to redirect the user to another page
// this is done using the history prop
export default withRouter(
    ({ course, history }) => {
        if (!course) {
            return <div>Loading ...</div>;
        }

        // redirects the user to the view page of the course, using the history prop
        const viewCourse = () => {
            history.push(`/courses/view/${course._id}`);
        }
        

        // shorten displayed course name, if it's too long
        const name = course.name.length < 20 ? course.name : course.name.slice(0, 17) + '...';

        return (
            <div className="course-presentation-item column is-one-third">
                <div className="card">
                    <div className="card-image" onClick={viewCourse}>
                        <figure className="image">
                            <img className="max-h-105" src={course.courseThumbnail} alt="Course Thumbnail" />
                        </figure>
                    </div>
                    <div className="card-content">
                        <div className="media">
                            <div className="media-content">
                                <p className="title is-4">{name}</p>
                                <p className="is-6">{course.professor.emails[0].address}</p>
                            </div>
                        </div>

                        <div className="content mb2">
                            <div className="level is-pulled-left has-text-primary">
                                {course.level}
                            </div>
                            <div className="price is-pulled-right">
                                {course.price}$
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
)