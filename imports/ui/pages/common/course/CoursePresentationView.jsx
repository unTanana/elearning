import React from 'react';
import CourseContents from './components/CourseContents.jsx';
import CoursePreview from './components/CoursePreview.jsx';
import { CardModal } from '/imports/ui/components/elements';
import { dispatch } from '/imports/store/store.js';

const reasonMap = {
    ALREADY_ENROLLED: 'You are already enrolled to this course.',
    NOT_LOGGED_IN: 'You need to log in first.',
    UNMET_PREREQUISITES: 'You need to finish other courses first.'
}

// presentation view component
export default class CoursePresentationView extends React.Component {
    // initial state of the component
    state = {
        course: null,
        activeTab: 'preview',
        activeCourseId: this.props.match.params.id,
        isEligible: false,
        reason: null,
        isCardModalActive: false
    }

    // allows rendering of another course, if the users clicks another one
    static getDerivedStateFromProps(nextProps, prevState) {
        // if another course is clicked
        if (nextProps.match.params.id !== prevState.activeCourseId || !prevState.course) {
            // reset the state
            return {
                course: null,
                isEligible: false,
                activeCourseId: nextProps.match.params.id
            }
        }

        return null;
    }

    // get the new course, if a user clicked another one
    componentDidUpdate() {
        // checks if the course has been reset, in the state
        if (this.state.course === null) {
            // if yes, get course date, and eligibility of the student, to it
            this.getCourse(this.state.activeCourseId)
            this.checkEligibility(this.state.activeCourseId);
        }
    }

    componentDidMount() {
        // get course data and eligibility of student, when the component mounts
        this.getCourse(this.state.activeCourseId);
        this.checkEligibility(this.state.activeCourseId);
    }

    // gets data of a course, by it's id
    getCourse(id) {
        // actual call to the sever, that gets the course
        Meteor.call('getCourseById', id, (err, res) => {
            if (!err) {
                this.setState({
                    course: res
                });
            }
        });
    }

    // checks eligibility of a student to a course, by course id
    checkEligibility(id) {
        // actual call to the server, to check eligibility
        Meteor.call('checkStudentEligibility', id, (err, res) => {
            console.log('Eligibility:', res);
            if (!err) {
                this.setState({
                    isEligible: res.eligibility,
                    reason: res.reason
                });
            }
        });
    }

    // enrolls student to a course
    enrollToCourse = (error) => {
        if (error) {
            alert('Something went wrong, please try again!');
            return;
        }

        // actual server call that enrolls the student to the selected course
        Meteor.call('enrollStudentToCourse', this.state.activeCourseId, (err) => {
            this.setState({
                isCardModalActive: false
            });

            if (!err) {
                // set state so the student sees that the enrollment was successfull
                this.setState({
                    isEligible: false,
                    reason: 'ALREADY_ENROLLED'
                });

                alert('You got the course!');
            } else {
                alert('Something went wrong');
            }

            // refresh course data
            this.getCourse(this.state.activeCourseId);
        });
    }

    // helper function determining active tab class
    getTabClass = (tab) => {
        return this.state.activeTab === tab ? 'is-active' : ''
    }

    // helper function setting the active tab
    setActiveTab = (tab) => {
        this.setState({
            activeTab: tab
        });
    }

    // helper function, toggles the card modal in the component state
    toggleCardModal = () => {
        this.setState({
            isCardModalActive: !this.state.isCardModalActive
        });
    }

    // starts the payment process, unless the course is free
    startPaymentProcess = () => {
        // if the course is free, don't process any payment
        if (this.state.course.price === 0) {
            if (confirm('Are you sure you want to enroll to this course?')) {
                this.enrollToCourse();
            }
            return;
        }

        // api call that generates the stripe payment intent code, on the server
        Meteor.call('createPaymentIntent', this.state.course.price, (err, intent) => {
            if (!err) {
                // save course price in store
                dispatch.course.updateSelectedCoursePrice(this.state.course.price);
                // save client secret in store
                dispatch.course.updateClientSecret(intent.client_secret);
                // toggle stripe card modal
                this.toggleCardModal();
            }
        });
    }

    // gets eligibility message from map, when this applies
    getEligibilityUnmetMessage = () => {
        const { reason } = this.state;

        // if there is a reason set
        if (reason) {
            return reasonMap[reason];
        }

        return null;
    }

    render() {
        const { course } = this.state;

        if (!course) {
            return <div> Loading ...</div>;
        }

        return (
            <div className="course-view">
                <article className="media">
                    <figure className="media-left">
                        <p className="image course-thumbnail">
                            <img src={course.courseThumbnail} alt="Course Thumbnail" />
                        </p>
                    </figure>
                    <div className="media-content">
                        <div className="content">
                            <p>
                                <strong className="mr1">{course.name}</strong>
                                <small>
                                    {
                                        course.professor.name || course.professor.emails[0].address
                                    }
                                </small>
                                <br />
                                Number of enrolled students:
                                <span className="ml05">
                                    {course.enrollmentCount || 0}
                                </span>
                            </p>
                            <p>
                                <strong className="mr1">Price: </strong> {course.price || 0}$
                            </p>
                        </div>
                    </div>
                </article>
                <div className="tabs mt2 is-boxed is-medium">
                    <ul>
                        <li className={this.getTabClass('preview')}>
                            <a onClick={() => this.setActiveTab('preview')}>
                                Preview
                            </a>
                        </li>
                        <li className={this.getTabClass('contents')}>
                            <a onClick={() => this.setActiveTab('contents')}>
                                Contents
                            </a>
                        </li>
                    </ul>
                </div>

                <div className="mt1 columns">
                    <div className="column is-four-fifths">
                        {
                            this.state.activeTab === 'preview'
                                ?
                                <CoursePreview course={course} />
                                :
                                <CourseContents course={course} />
                        }
                    </div>
                    <div className="column is-one-fifth enrollment-area">
                        <CardModal
                            isActive={this.state.isCardModalActive}
                            onSubmit={this.enrollToCourse}
                            closeModal={this.toggleCardModal}
                        />

                        <button
                            className="button is-link"
                            disabled={!this.state.isEligible}
                            type="button"
                            onClick={this.startPaymentProcess}
                        >
                            Enroll
                        </button>

                        <div className="subtitle mt1">
                            {this.getEligibilityUnmetMessage()}
                        </div>

                    </div>
                </div>
            </div>
        )
    }
}