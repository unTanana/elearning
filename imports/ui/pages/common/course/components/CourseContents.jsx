import React from 'react';
import { Accordion } from 'react-accessible-accordion';
import LessonItem from './LessonItem.jsx';
import 'react-accessible-accordion/dist/fancy-example.css'; // import css of accordion

// connect to store, to get the coursesCache
export default (props) => {
    const { course } = props;

    return (
        <div className="course-contents">
            <Accordion allowZeroExpanded={true} allowMultipleExpanded={true}>
                {
                    course.videos.map(video => (
                        <LessonItem key={video.url} video={video} />
                    ))
                }
            </Accordion>
        </div>
    );
}