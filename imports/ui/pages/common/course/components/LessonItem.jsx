import React from 'react';
import {
    AccordionItem,
    AccordionItemHeading,
    AccordionItemPanel,
    AccordionItemButton
} from 'react-accessible-accordion';
import { VideoPlayer } from '/imports/ui/components/elements';

// renders each Lesson as an AccordionItem
export default (props) => {
    const { video } = props;
    return (

        <AccordionItem>
            <AccordionItemHeading>
                <AccordionItemButton>
                    {video.name}
                </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel>
                <p>
                    {video.description}
                </p>
                {
                    !video.isLocked &&
                    <div className="lesson-video-container mt1">
                        <VideoPlayer
                            src={video.url}
                            thumbnail={video.thumbnail}
                        />
                    </div>
                }
            </AccordionItemPanel>
        </AccordionItem>
    )
}