import React from 'react';
import { connect } from 'react-redux';
import { LinkButton } from '/imports/ui/components/elements'

// connects component to the store, to get the courses cache
// courses cache is used to get the name of prerequisite courses
export default connect(
    state => ({
        coursesCache: state.course.coursesCache
    })
)((props) => {
    const { course, coursesCache } = props;

    let formattedPrerequisites;

    if (!coursesCache) {
        return <div>Loading ...</div>
    }

    // format prerequisites before displaying them
    // get name of a course, from the cache
    if (course.prerequisites) {
        const { prerequisites } = course;
        formattedPrerequisites = coursesCache.filter(course => (
            prerequisites.indexOf(course._id) > -1
        )).map(course => ({ name: course.name, _id: course._id }))
    }

    return (
        <div className="course-preview">
            <div className="columns jc-sb">
                <div className="column is-one-third">
                    <p className="title">What you will learn</p>

                    {
                        formattedPrerequisites &&
                        <>
                            <header className="card-header">
                                <p className="card-header-title">
                                    Prerequisites:
                                </p>
                            </header>
                            <div className="card-content">
                                <div className="content">
                                    {
                                        formattedPrerequisites.map(prerequisite => (
                                            <LinkButton
                                                key={prerequisite._id}
                                                to={`/courses/view/${prerequisite._id}`}
                                                className="prerequisite-link"
                                            >
                                                <p >{prerequisite.name}</p>
                                            </LinkButton>
                                        ))
                                    }
                                </div>
                            </div>
                        </>
                    }

                    <header className="card-header">
                        <p className="card-header-title">
                            Description:
                        </p>
                    </header>

                    <div className="card-content">
                        <div className="content">
                            <p>
                                {course.description}
                            </p>
                        </div>
                    </div>
                </div>
                <div className="column is-one-third">
                    <p className="title">Content preview</p>

                    <header className="card-header">
                        <p className="card-header-title">
                            Lessons:
                        </p>
                    </header>
                    <div className="card-content">
                        <div className="content">
                            {
                                course.videos.map((video, index) => {
                                    if (!video.isLocked) {
                                        return (
                                            <p
                                                key={index}
                                            >
                                                {video.name}
                                            </p>
                                        )
                                    } else {
                                        return (
                                            <p
                                                key={index}
                                            >
                                                Lesson Locked
                                            </p>
                                        )
                                    }
                                })
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
})