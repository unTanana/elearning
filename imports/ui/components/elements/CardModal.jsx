import React from 'react';
import Modal from './Modal.jsx';
import { connect } from 'react-redux';

// initialize stripe
const stripe = window.Stripe(Meteor.settings.public.stripe.dev.publicKey)

// initialize stripe elements
const elements = stripe.elements()

// create card element
const card = elements.create('card', {
    style: {
        base: {
            iconColor: '#666EE8',
            color: '#31325F',
            fontWeight: 300,
            fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
            fontSize: '15px',
            '::placeholder': {
                color: '#CFD7E0',
            },
        },
    }
});

// connect to the store
export default connect(
    state => ({
        selectedCoursePrice: state.course.selectedCoursePrice,
        clientSecret: state.course.clientSecret
    })
)(class CardModal extends React.Component {
    state = {
        hasCardError: false
    }

    handleStripeCall = async () => {
        const { clientSecret } = this.props; // get client secret, generated on the server

        // get results from stripe payment
        const { paymentIntent, error } = await stripe.handleCardPayment(
            clientSecret, card, {
                payment_method_data: {
                    billing_details: { name: Meteor.user().profile && Meteor.user().profile.name || "Test Student" }
                }
            }
        );

        this.props.onSubmit(error); // execute callback
    }

    componentDidMount() {
        // mount card element on component startup
        card.mount('#card-element');
        // listen to change events in the card form
        card.addEventListener('change', event => {
            if (event.error) {
                this.setState({
                    hasCardError: true
                });
            } else {
                this.setState({
                    hasCardError: false
                });
            }
        });
    }

    render() {
        return (
            <Modal
                title={`Course - ${this.props.selectedCoursePrice}$`}
                isActive={this.props.isActive}
                closeModal={this.props.closeModal}
            >
                <div id='card-element' />
                <footer className="mt2">
                    <button
                        ref="cardButton"
                        className="button is-link mr2"
                        id="card-button"
                        data-secret="{{ client_secret }}"
                        onClick={this.handleStripeCall}
                        disabled={this.state.hasCardError}
                    >
                        Get course
                    </button>
                    <button className="button" type="button" onClick={this.props.closeModal}>Cancel</button>
                </footer>
            </Modal>
        )
    }

})