import React from 'react';
import { Player, BigPlayButton, LoadingSpinner } from 'video-react';
import 'video-react/dist/video-react.css'; // import css of video player

export default class VideoPlayer extends React.Component {
    componentDidMount() {
        // subscribe state change
        if (this.props.onStateChange) {
            this.refs.player.subscribeToStateChange(this.props.onStateChange);
        }
    }

    render() {
        // get passed settings
        const { autoPlay, muted, src, thumbnail } = this.props;

        return (
            <Player
                autoPlay={autoPlay}
                muted={muted}
                poster={thumbnail}
                ref="player"
            >
                <source src={src} />
                <BigPlayButton position="center" />
                <LoadingSpinner />
            </Player>
        )
    }
}
