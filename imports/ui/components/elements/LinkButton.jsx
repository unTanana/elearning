import React from 'react';
import { Link } from 'react-router-dom';

// button that redirects to a path
export default (props) => {
    const { to, children, className } = props;
    const linkClass = `button is-link ${className}`;
    
    return (
        <Link className={linkClass} to={to}>
            {children}
        </Link>
    )
}