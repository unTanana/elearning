import React from 'react';

// generic modal component
export default (props) => {
    const modalClass = props.isActive ? 'modal is-active' : 'modal';

    return (
        <div className={modalClass}>
            <div className="modal-background" onClick={props.closeModal}></div>
            <div className="modal-card">
                {
                    props.title
                        ?
                        <header className="modal-card-head">
                            <p className="modal-card-title">{props.title}</p>
                            <button className="delete" aria-label="close" onClick={props.closeModal}></button>
                        </header>
                        :
                        <button className="delete" aria-label="close" onClick={props.closeModal}></button>

                }

                <section className="modal-card-body">
                    {props.children}
                </section>
                {
                    props.onSubmit &&
                    <footer className="modal-card-foot">
                        <button className="button is-success">{props.yesText || "Save Changes"}</button>
                        <button className="button" type="button" onClick={props.closeModal}>{props.noText || "Cancel"}</button>
                    </footer>
                }

            </div>
        </div>
    )
}