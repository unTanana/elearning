import React from 'react';
import cx from 'classnames';
import { Field, FastField, getIn } from 'formik';
import SelectSearch from 'react-select-search'

const SelectSearchComponent = ({
    field, // { name, value, onChange, onBlur }
    form, //touched, errors, also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
    className,
    options,
    ...props
}) => {
    const { touched, errors, setFieldValue, setFieldTouched } = form;
    let value = field.value;
    const errorMessage = getIn(errors, field.name)
    const hasError = getIn(errors, field.name) && getIn(touched, field.name);

    // handle field change manually
    const onChangeHandler = (selected, state) => {
        setFieldValue(field.name, state.value);
    }

    // handle blur change manually
    const onBlurHandler = () => {
        setFieldTouched(field.name, true);
    }

    return (
        <div className={cx('field', className, {
            'has-error': hasError
        })}>
            {props.label && <label className="label">{props.label}</label>}
            <SelectSearch
                options={options}
                value={value}
                name={field.name}
                placeholder={props.placeholder}
                {...props}
                onChange={onChangeHandler}
                onBlur={onBlurHandler}
            />
            {hasError && <div className="error-field">{errorMessage}</div>}
            <span />
        </div>
    )
}

// wrap a component in Field or FastField component, to connect it to a formik form
export default ({ name, isSlow, ...props }) => {
    if (isSlow) {
        // Works slower than FastField, but handles form state changes perfectly
        return <Field name={name} component={SelectSearchComponent} {...props} />
    } else {
        // Fast Field is default, and works faster, but doesn't handle form state changes as well as Field
        return <FastField name={name} component={SelectSearchComponent} {...props} />
    }
};