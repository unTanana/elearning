import React from 'react';
import cx from 'classnames';
import { Field, FastField, getIn } from 'formik';

const SelectComponent = ({
    field, // { name, value, onChange, onBlur }
    form, //touched, errors, also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
    className,
    options,
    ...props
}) => {
    const { touched, errors } = form;
    let value = field.value;
    const errorMessage = getIn(errors, field.name)
    const hasError = getIn(errors, field.name) && getIn(touched, field.name);

    return (
        <div className={cx('field', className, {
            'has-error': hasError
        })}>
            <div className="control">
                {props.label && <label className="label">{props.label}</label>}
                <div className="select">
                    <select className={props.classSelect} {...field} {...props}>
                        {(props.placeholder && (value === 'default' || props.showPlaceholder)) && <option value="default" disabled>{props.placeholder}</option>}
                        {options.map(({ value, title, disabled }) => (
                            <option value={value} key={value} disabled={disabled}>{title}</option>
                        ))}
                    </select>
                </div>
            </div>
            {hasError && <div className="error-field">{errorMessage}</div>}
            <span />
        </div>
    )
}

// wrap a component in Field or FastField component, to connect it to a formik form
export default ({ name, isSlow, ...props }) => {
    if (isSlow) {
        // Works slower than FastField, but handles form state changes perfectly
        return <Field name={name} component={SelectComponent} {...props} />
    } else {
        // Fast Field is default, and works faster, but doesn't handle form state changes as well as Field
        return <FastField name={name} component={SelectComponent} {...props} />
    }
};