import React from 'react';
import cx from 'classnames';
import { Field, FastField, getIn } from 'formik';

const CheckboxComponent = ({
    field, // { name, value, onChange, onBlur }
    form, //touched, errors, also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
    className,
    ...props
}) => {
    const { touched, errors } = form;
    const value = field.value;
    const errorMessage = getIn(errors, field.name);
    const hasError = errorMessage && getIn(touched, field.name);
    const hasValue = value !== "" && value !== undefined && value !== null;
    return (
        <div className="field">
            <label className="checkbox">
                <input
                    className={
                        cx('checkbox', 'mr1', className, {
                            'has-error': hasError,
                            'has-value': hasValue,
                            'disabled': props.disabled
                        })
                    }
                    type="checkbox"
                    checked={value}
                    {...field}
                    {...props}
                >
                </input>
                {props.label}
            </label>
            {hasError && <div className="error-field">{errorMessage}</div>}
        </div>
    )
};

// wrap a component in Field or FastField component, to connect it to a formik form
export default ({ name, isSlow, ...props }) => {
    if (isSlow) {
        // Works slower than FastField, but handles form state changes perfectly
        return <Field name={name} component={CheckboxComponent} {...props} />
    } else {
        // Fast Field is default, and works faster, but doesn't handle form state changes as well as Field
        return <FastField name={name} component={CheckboxComponent} {...props} />
    }
};