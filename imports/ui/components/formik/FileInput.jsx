import React from 'react';
import cx from 'classnames';
import { Field, FastField, getIn } from 'formik';
import { Meteor } from 'meteor/meteor';
import { Slingshot } from 'meteor/edgee:slingshot';


class UploadComponent extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            uploading: false,
            uploaded: false || props.field.value,
            error: false,
            fileName: '',
            fileUrl: '' || props.field.value,
            uploadProgress: 0
        }
        this._intervalId = null;
        // initialize upload helper
        this._uploader = new Slingshot.Upload(props.uploadTarget);

    }

    // used to upload the file to s3, set the state of the component, and then set the filed in the form
    handleFile = (event) => {
        const self = this
        const file = event.target.files[0]
        const name = file.name

        this.setState({
            uploading: true
        })

        // update state with upload progress, every 100ms
        this._intervalId = setInterval(() => {
            this.setState({
                uploadProgress: Math.round(this._uploader.progress() * 100) || 0
            });
        }, 100);


        // does the actual uploading to s3
        this._uploader.send(file, (err, url) => {
            console.log('upload err:', err)

            const _state = err ? {
                uploading: false,
                uploaded: false,
                error: false,
                fileName: '',
                fileUrl: '',
                uploadProgress: 0
            } : {
                    uploading: false,
                    uploaded: true,
                    fileName: name,
                    fileUrl: url
                }

            // sets the state of the component
            self.setState(_state);
            clearInterval(self._intervalId);
            if (!err) {
                // sets the url as the value of the form
                this.props.form.setFieldValue(this.props.field.name, url);
            }
        })
    }

    // calls the meteor method that deletes a file from s3, using the url of the file
    deleteFile = () => {
        const isVideo = this.props.uploadTarget.includes('video');
        const isThumbnail = this.props.uploadTarget.includes('thumbnail');
        const deletionMethod = isVideo
            ?
            'deleteVideoFromStorage' :
            isThumbnail
                ? 'deleteThumbnailFromStorage'
                : 'deleteAttachmentFromStorage';
        // the actual call
        Meteor.call(deletionMethod, this.state.fileUrl, (err, res) => {
            if (!err) {
                console.log('file deleted succesfully');
                this.setState({
                    uploading: false,
                    uploaded: false,
                    fileName: '',
                    fileUrl: '',
                    error: false,
                    uploadProgress: 0
                });
            }
        });
    }

    render() {

        const { field, form, className, label } = this.props;
        const { touched, errors } = form;
        const value = field.value;
        const errorMessage = getIn(errors, field.name);
        const hasError = errorMessage && getIn(touched, field.name);
        const hasValue = value !== "" && value !== undefined && value !== null;

        const { uploading, uploaded, fileName } = this.state
        return (
            <div className="field">
                <div className="control">
                    {
                        label &&
                        <label className="label">{label}</label>
                    }
                    {
                        uploaded
                            ?
                            <div>
                                <em>File Uploaded</em>
                                <span className="delete x-button ml1" onClick={this.deleteFile}></span>
                            </div>
                            :
                            <input
                                className={
                                    cx('input', className, {
                                        'has-error': hasError,
                                        'has-value': hasValue,
                                        'disabled': this.props.disabled
                                    })
                                }
                                {...this.props}
                                type='file'
                                name={field.name}
                                onBlur={field.onBlur}
                                disabled={uploading}
                                onChange={this.handleFile}
                            >
                            </input>
                    }
                    {
                        this.state.uploading &&
                        <progress className="progress is-small is-primary" value={this.state.uploadProgress} max="100">{this.state.uploadProgress}%</progress>
                    }
                    {hasError && <div className="error-field">{errorMessage}</div>}
                </div>
            </div>
        )
    }
}

// wrap a component in Field or FastField component, to connect it to a formik form
export default ({ name, isSlow, ...props }) => {
    if (isSlow) {
        // Works slower than FastField, but handles form state changes perfectly
        return <Field name={name} component={UploadComponent} {...props} />
    } else {
        // Fast Field is default, and works faster, but doesn't handle form state changes as well as Field
        return <FastField name={name} component={UploadComponent} {...props} />
    }
};