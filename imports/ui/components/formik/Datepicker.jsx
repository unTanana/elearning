import React from 'react';
import { Field, FastField, getIn } from 'formik';
import DatePicker from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";

const DatepickerComponent = ({
    field, // { name, value, onChange, onBlur }
    form, //touched, errors, also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
    className,
    ...props
}) => {
    const { touched, errors, setFieldValue, setFieldTouched } = form;
    const value = field.value;
    const errorMessage = getIn(errors, field.name);
    const hasError = errorMessage && getIn(touched, field.name);

    // manually handle value change
    const onChangeHandler = (value) => {
        setFieldValue(field.name, value);
    }

    // manually handle blur change
    const onBlurHandler = () => {
        setFieldTouched(field.name, true);
    }

    return (
        <div className="field">
            <div className="control">
                {props.label && <label className="label">{props.label}</label>}
                <DatePicker
                    className={`input ${className}`}
                    selected={value}
                    onChange={onChangeHandler}
                    dateFormat="MMMM d, yyyy HH:mm"
                    showTimeSelect
                    timeFormat="HH:mm"
                    onBlur={onBlurHandler}
                    placeholderText={props.placeholder}
                    withPortal
                />
                {hasError && <div className="error-field">{errorMessage}</div>}
            </div>
        </div>
    )
};

// wrap a component in Field or FastField component, to connect it to a formik form
export default ({ name, isSlow, ...props }) => {
    if (isSlow) {
        // Works slower than FastField, but handles form state changes perfectly
        return <Field name={name} component={DatepickerComponent} {...props} />
    } else {
        // Fast Field is default, and works faster, but doesn't handle form state changes as well as Field
        return <FastField name={name} component={DatepickerComponent} {...props} />
    }
};