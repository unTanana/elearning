import React, { Component } from 'react';
import { connect } from 'react-redux';
import { CATEGORIES, LEVELS } from '/imports/api/courses/values.js';

// course filters component, visible on homepage
// connects to the store, to get selected plugins
export default connect(
    state => ({
        course: state.course
    }),
    dispatch => ({
        // helper functions (reducers) that update the store with new filters
        updateCategory: dispatch.course.updateCategory,
        updateLevel: dispatch.course.updateLevel,
        updateSearchString: dispatch.course.updateSearchString,
        updatePriceMin: dispatch.course.updatePriceMin,
        updatePriceMax: dispatch.course.updatePriceMax,
        toggleFiltersMenu: dispatch.course.toggleFiltersMenu
    })
)(class CourseFilters extends Component {

    toggleMenu = (e) => {
        e.stopPropagation(); // stop the event from triggering other elements below the clicked one
        this.props.toggleFiltersMenu();
    }

    // determines if a category filter is active
    isCategoryActive = (category) => {
        const { categories } = this.props.course;

        return categories.indexOf(category) !== -1;
    }

    // determines if a level filter is active
    isLevelActive = (level) => {
        const { levels } = this.props.course;

        return levels.indexOf(level) !== -1;
    }

    // when inserting a search string, updates the model state
    onSearchChange = (e) => {
        this.props.updateSearchString(e.target.value);
    }

    // handles minimum price change, updates the model state
    onMinimumChange = (e) => {
        const number = parseFloat(e.target.value) || 0;
        this.props.updatePriceMin(number);
    }

    // handles maximum price change, updates the model state
    onMaximumChange = (e) => {
        const number = parseFloat(e.target.value) || 0;
        this.props.updatePriceMax(number);
    }

    render() {
        const { isFiltersMenuVisible, priceMin, priceMax } = this.props.course;

        return (
            <div className="course-filters mb2">
                <div className="columns">
                    <div className="flex-grow-0 column">
                        <div className="toggle has-text-danger" onClick={this.toggleMenu}>
                            Filters
                        </div>
                    </div>
                    <div className="column is-three-fifths">
                        <div className="field">
                            <div className="control">
                                <input
                                    className="input is-primary"
                                    type="text"
                                    placeholder="Search"
                                    onChange={this.onSearchChange}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="column is-one-fifth">
                        <button
                            className="button is-primary"
                            onClick={this.props.filterCourses}
                            type="button"
                        >
                            SEARCH
                        </button>
                    </div>
                </div>

                <aside
                    className={`card menu ${isFiltersMenuVisible ? 'is-visible' : ''}`}
                    onClick={(e) => { e.stopPropagation() }}
                >
                    <p className="menu-label">
                        Categories
                    </p>
                    <ul className="menu-list">
                        {
                            CATEGORIES.map(category => (
                                <li
                                    key={category.value}
                                    onClick={this.props.updateCategory.bind(this, category.value)}
                                >
                                    <a className={this.isCategoryActive(category.value) ? 'is-active' : ''}>
                                        {category.value}
                                    </a>
                                </li>
                            ))
                        }
                    </ul>
                    <p className="menu-label">
                        Levels
                    </p>
                    <ul className="menu-list">
                        {
                            LEVELS.map(level => (
                                <li
                                    key={level.value}
                                    onClick={this.props.updateLevel.bind(this, level.value)}
                                >
                                    <a className={this.isLevelActive(level.value) ? 'is-active' : ''}>
                                        {level.value}
                                    </a>
                                </li>
                            ))
                        }
                    </ul>
                    <p className="menu-label">
                        Pricing
                    </p>
                    <div className="field">
                        <div className="control">
                            <label className="label">Minimum Price</label>
                            <input
                                className="input is-primary"
                                type="number"
                                placeholder="Minimum"
                                value={priceMin || ''}
                                onChange={this.onMinimumChange}
                            />
                        </div>
                    </div>
                    <div className="field">
                        <div className="control">
                            <label className="label">Maximum Price</label>
                            <input
                                className="input is-primary"
                                type="number"
                                placeholder="Maximum"
                                value={priceMax || ''}
                                onChange={this.onMaximumChange}
                            />
                        </div>
                    </div>
                </aside>
            </div>
        );
    }
});