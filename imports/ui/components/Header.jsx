import React from 'react';
import { Link } from 'react-router-dom';

// header component, visible on all pages
export default class Header extends React.Component {
    state = {
        active: false
    }

    // handles burger menu toggle, when in mobile form
    toggleMenu = () => {
        this.setState({
            active: !this.state.active
        })
    }

    render() {
        const { user } = this.props;
        const isAdmin = user && Roles.userIsInRole(user._id, ['administrator']);
        const isProfessor = user && Roles.userIsInRole(user._id, ['professor']);
        const isStudent = user && Roles.userIsInRole(user._id, ['student']);

        return (
            <nav className="navbar" role="navigation" aria-label="main navigation">
                <div className="navbar-brand">
                    <Link to="/" className="navbar-item">
                        LOGO
                    </Link>
                    <a
                        role="button"
                        onClick={this.toggleMenu}
                        className={`navbar-burger ${this.state.active ? 'is-active' : ''}`}
                        aria-label="menu"
                        aria-expanded="false"
                    >
                        <span aria-hidden="true"></span>
                        <span aria-hidden="true"></span>
                        <span aria-hidden="true"></span>
                    </a>
                </div>

                <div className={`navbar-menu ${this.state.active ? 'is-active' : ''}`}>
                    <div className="navbar-end">
                        {
                            user ?
                                <>
                                    {
                                        isAdmin &&
                                        <Link to="/dashboard" className="navbar-item">
                                            Dashboard
                                    </Link>
                                    }

                                    {
                                        isProfessor &&
                                        <Link to="/my-courses" className="navbar-item">
                                            My Courses
                                    </Link>
                                    }

                                    {
                                        isStudent &&
                                        <Link to="/library" className="navbar-item">
                                            My Library
                                    </Link>
                                    }

                                    <Link to="/my-profile" className="navbar-item">
                                        My profile
                                </Link>
                                    <Link to="/logout" className="navbar-item">
                                        Logout
                                </Link>
                                </> :
                                <>
                                    <Link to="/login" className="navbar-item">
                                        Login
                                </Link>
                                    <Link to="/register" className="navbar-item">
                                        Register
                                </Link>
                                </>
                        }
                    </div>
                </div>
            </nav>
        );
    }
}
