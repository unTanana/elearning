import React from "react";
import { BrowserRouter as Router, Route, Redirect, Switch } from "react-router-dom";
import DashboardComponent from './../ui/pages/admin/Dashboard.jsx';
import CoursesPresentationPageComponent from './../ui/pages/CoursesPresentationPage.jsx';
import LoginComponent from './../ui/pages/Login.jsx';
import RegisterApplicationsComponent from './../ui/pages/common/applications/ApplicationForm.jsx';
import ProfessorCoursesComponent from './../ui/pages/professor/MyCourses.jsx';
import ProfessorCoursesFormComponent from './../ui/pages/professor/CourseForm.jsx';
import ProfessorCoursesViewComponent from './../ui/pages/professor/CourseProfessorView.jsx';
import CoursePresentationViewComponent from './../ui/pages/common/course/CoursePresentationView.jsx';
import StudentLibraryComponent from './../ui/pages/student/StudentLibrary.jsx';
import StudentLessonViewComponent from './../ui/pages/student/StudentLessonView.jsx';
import ApplicationAccepted from '/imports/ui/pages/application/ApplicationAccepted.jsx';
import MyProfileComponent from '/imports/ui/pages/common/profile/MyProfile.jsx';
import Header from './../ui/components/Header.jsx';
import { Meteor } from 'meteor/meteor';

// routes that are only accessible if you are not logged in
const GuestRoute = ({ component: Component, ...rest }) => {
    return (
        <Route
            {...rest}
            render={props =>
                !rest.user ? (
                    <Component {...props} />
                ) : (
                        <Redirect
                            to={{
                                pathname: "/",
                            }}
                        />
                    )
            }
        />
    );
}

// routes that are only accessible if you are logged in, and an ADMINISTRATOR.
const AdminRoute = ({ component: Component, ...rest }) => {
    const { user } = rest;
    const isAdmin = user && Roles.userIsInRole(user._id, ['administrator']);
    return (
        <Route
            {...rest}
            render={props =>
                isAdmin ? (
                    <Component {...props} />
                ) : (
                        <Redirect
                            to={{
                                pathname: "/",
                            }}
                        />
                    )
            }
        />
    );
}

// routes that are only accessible if you are logged in, and a PROFESSOR or ADMINISTRATOR.
const ProfessorRoute = ({ component: Component, ...rest }) => {
    const { user } = rest;
    const isAdminOrProfessor = user && Roles.userIsInRole(user._id, ['administrator', 'professor']);
    return (
        <Route
            {...rest}
            render={props =>
                isAdminOrProfessor ? (
                    <Component {...props} />
                ) : (
                        <Redirect
                            to={{
                                pathname: "/",
                            }}
                        />
                    )
            }
        />
    );
}

// routes that are only accessible if you are logged in, and a STUDENT or ADMINISTRATOR.
const StudentRoute = ({ component: Component, ...rest }) => {
    const { user } = rest;
    const isAdminOrStudent = user && Roles.userIsInRole(user._id, ['administrator', 'student']);
    return (
        <Route
            {...rest}
            render={props =>
                isAdminOrStudent ? (
                    <Component {...props} />
                ) : (
                        <Redirect
                            to={{
                                pathname: "/",
                            }}
                        />
                    )
            }
        />
    );
}

// routes that are only accessible if you are logged in
const LoggedInRoute = ({ component: Component, ...rest }) => {
    return (
        <Route
            {...rest}
            render={props =>
                rest.user ? (
                    <Component {...props} />
                ) : (
                        <Redirect
                            to={{
                                pathname: "/",
                            }}
                        />
                    )
            }
        />
    );
}

const MainRouter = (props) => {
    const { user } = props;
    // router definition, containing all routes available on the website
    return (
        <Router>
            <div>
                <Header user={user} />
                <div className="section">
                    <Switch>
                        <Route exact path="/" component={CoursesPresentationPageComponent} user={user} />
                        <Route exact path="/courses/view/:id" component={CoursePresentationViewComponent} user={user} />

                        <LoggedInRoute exact path="/my-profile" component={MyProfileComponent} user={user} />

                        <GuestRoute path="/register" component={RegisterApplicationsComponent} user={user} />
                        <GuestRoute path="/login" component={LoginComponent} user={user} />
                        <GuestRoute path="/applications/accepted" component={ApplicationAccepted} user={user} />

                        <StudentRoute exact path="/library" component={StudentLibraryComponent} user={user} />
                        <StudentRoute exact path="/library/view/:id" component={StudentLessonViewComponent} user={user} />

                        <ProfessorRoute exact path="/my-courses" component={ProfessorCoursesComponent} user={user} />
                        <ProfessorRoute exact path="/my-courses/create" component={ProfessorCoursesFormComponent} user={user} />
                        <ProfessorRoute exact path="/my-courses/view/:id" component={ProfessorCoursesViewComponent} user={user} />
                        <ProfessorRoute exact path="/my-courses/edit/:id" component={ProfessorCoursesFormComponent} user={user} />

                        <AdminRoute path="/dashboard" component={DashboardComponent} user={user} />

                        <Route path="/logout" render={() => {
                            //logs out the user, and then proceeds to login page
                            Meteor.logout();
                            return <Redirect to='/login' />
                        }} />

                        <Route render={() => {
                            // when accessing a non existing router, get redirected to the homepage
                            return <Redirect to='/' />
                        }} />
                    </Switch>
                </div>
            </div>
        </Router>
    );
}



export default MainRouter;