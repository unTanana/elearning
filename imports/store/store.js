import { init } from '@rematch/core'
import createRematchPersist from '@rematch/persist'
import * as models from './models'

// initializes persistence of the courses model
// allows the store to remember filters after leaving the page
const persistPlugin = createRematchPersist({
    whitelist: ['courses'],
    throttle: 500,
    version: 1,
})

// attaches the persistence plugin to rematch
init({
    plugins: [persistPlugin]
})

// initializes the store, with models and plugins
const store = init({
    models,
    plugins: [persistPlugin]
})

export default store;
export const { dispatch } = store