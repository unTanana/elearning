// site
export default {
    state: { // initial state
        categories: [],
        levels: [],
        searchString: '',
        priceMin: null,
        priceMax: null,
        isFiltersMenuVisible: false,
        coursesCache: [],
        selectedCoursePrice: null,
        clientSecret: null,
        selectedCourseProgress: 0
    },
    reducers: { // functions that alter the state
        updateCategory(state, category) {
            if (state.categories.indexOf(category) === -1) {
                state.categories.push(category);
            } else {
                const categoryIndex = state.categories.indexOf(category);
                state.categories.splice(categoryIndex, 1);
            }
            return {
                ...state
            };
        },
        updateLevel(state, level) {
            if (state.levels.indexOf(level) === -1) {
                state.levels.push(level);
            } else {
                const levelIndex = state.levels.indexOf(level);
                state.levels.splice(levelIndex, 1);
            }
            return {
                ...state
            };
        },
        updateSearchString(state, searchString) {
            state.searchString = searchString;

            return {
                ...state
            };
        },
        updatePriceMin(state, priceMin) {
            state.priceMin = priceMin;

            return {
                ...state
            };
        },
        updatePriceMax(state, priceMax) {
            state.priceMax = priceMax;

            return {
                ...state
            };
        },
        toggleFiltersMenu(state) {
            state.isFiltersMenuVisible = !state.isFiltersMenuVisible;

            return {
                ...state
            };
        },

        updateCoursesCache(state, courses) {
            state.coursesCache = courses;

            return {
                ...state
            };
        },

        updateSelectedCoursePrice(state, price) {
            state.selectedCoursePrice = price;

            return {
                ...state
            };
        },

        updateSelectedCourseProgress(state, progress) {
            state.selectedCourseProgress = progress;

            return {
                ...state
            };
        },

        updateClientSecret(state, secret) {
            state.clientSecret = secret;

            return {
                ...state
            };
        }
    },
}