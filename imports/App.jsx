import React from 'react';
import Router from './router/Router.jsx';
import { withTracker } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';
import { Provider } from 'react-redux';
import store from './store/store.js';
import { getPersistor } from '@rematch/persist';
import { PersistGate } from 'redux-persist/lib/integration/react';
import { dispatch } from '/imports/store/store.js';

// main App stub, all components are mounted inside of this component.
const App = (props) => {
    return (
        <div id='App'>
            <Router user={props.user} />
        </div>
    )
}

// getting persistence plugin
const persistor = getPersistor();

// wrap the App stub inside the persistor plugin
const Wrapper = (props) => {
    return (
    <Provider store={store}>
        <PersistGate persistor={persistor}>
            <App {...props} />
        </PersistGate>
    </Provider>
    )
};

// wrap everything in a subscription to the currently logged in user
// so it can be accessed everywhere in the app
export default withTracker(props => {
    // Do all your reactive data access in this method.
    // Note that this subscription will get cleaned up when your component is unmounted
    Meteor.subscribe('root', props.id);

    // fill courses cache
    Meteor.call('getCoursesCache', (err, res) => {
        if (!err) {
            dispatch.course.updateCoursesCache(res);
        }
    });

    return {
        user: Meteor.user(),
    };
})(Wrapper);