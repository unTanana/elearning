import { Mongo } from 'meteor/mongo';

// creating the applications collection
const Applications = new Mongo.Collection('applications');

export default Applications;


/*

SCHEMA

_id
name,
email,
about (short description)
type: student, professor
status: pending, accepted, rejected, confirmed, archived (todo)

*/