import { Meteor } from 'meteor/meteor';
import Applications from './applications.js';
import { Email } from 'meteor/email';
import emailAccepted from '/imports/server/email/templates/applicationAccepted.js';
import emailDeclined from '/imports/server/email/templates/applicationDeclined.js';

// methods to manipulate the applications collection
Meteor.methods({
    registerApplication(application) {
        application.status = 'pending';

        return Applications.insert(application);
    },

    approveApplication(applicationId) {
        const isAdmin = Roles.userIsInRole(this.userId, ['administrator']);

        // must be an admin to approve an application
        if (!isAdmin) {
            return;
        }

        const application = Applications.findOne(applicationId);

        Applications.update(applicationId, {
            $set: {
                status: 'approved'
            }
        });

        Email.send({
            from: 'elearning@admin.com',
            to: application.email,
            subject: 'Accepted',
            html: emailAccepted({ acceptedLink: 'http://localhost:3000/applications/accepted' }),
        });
    },

    rejectApplication(applicationId) {
        const isAdmin = Roles.userIsInRole(this.userId, ['administrator']);

        // must be an admin to reject an application
        if (!isAdmin) {
            return;
        }

        const application = Applications.findOne(applicationId);

        Applications.update(applicationId, {
            $set: {
                status: 'rejected'
            }
        });

        Email.send({
            from: 'elearning@admin.com',
            to: application.email,
            subject: 'Declined',
            html: emailDeclined(),
        });
    },

    getApplications(filters) {
        const isAdmin = Roles.userIsInRole(this.userId, ['administrator']);

        // must be an admin to see applications
        if (!isAdmin) {
            return;
        }

        const filtersObject = {};

        // filter applications
        if (filters.status) {
            filtersObject.status = filters.status;
        }

        if (filters.type) {
            filtersObject.type = filters.type;
        }

        return Applications.find(filtersObject).fetch();
    }
});