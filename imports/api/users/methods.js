import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base'
import { Roles } from 'meteor/alanning:roles'
import Stripe from 'stripe';
import Application from '/imports/api/applications/applications.js';
import uuidv1 from 'uuid/v1'; // used for generating registration numbers

// stripe object handler initialization
const stripe = Stripe(Meteor.settings.stripe.dev.privateKey);

// Meteor methods concerning users
Meteor.methods({
    // returns a stripe payment intent id
    // used to fulfill a payment on the client side
    createPaymentIntent(amount) {
        return stripe.paymentIntents.create({
            amount: amount * 100, // * 100 to transform cents to dollars
            currency: 'usd',
        });
    },

    // returns the status of an application based on it's email
    checkApplicationEmail({ email }) {
        const application = Application.findOne({ email });
        const user = Meteor.user({ 'emails.address': email });

        if (user) {
            return {
                success: false,
                error: 'It looks like you are already registered.'
            }
        }

        if (!application) {
            return {
                success: false,
                error: 'There is no application with this email.'
            }
        }

        if (application.status !== 'approved') {
            return {
                success: false,
                error: 'Your application was not approved.'
            }
        }

        return {
            success: true,
            applicationId: application._id,
            error: null
        };
    },

    // registers a new user, if possible
    registerApplicationUser({ applicationId, password }) {
        const application = Application.findOne(applicationId);
        const user = Meteor.user({ 'emails.address': application.email });

        if (user) {
            throw new Meteor.Error('User already exists');
        }

        if (application.status !== 'approved') {
            throw new Meteor.Error('Application not approved');
        }

        // create the user
        const userId = Accounts.createUser({
            email: application.email,
            password
        });

        // add the role
        Roles.addUsersToRoles(userId, [application.type]);

        // fill in the rest of the data
        const updateObject = {
            'profile.name': application.name,
            'profile.about': application.about,
        }

        // generate a registration number, if it's a student
        if (application.type === 'student') {
            updateObject.registrationNumber = uuidv1();
        }

        // update the new user
        Meteor.users.update(userId, {
            $set: updateObject
        })

        return userId;
    },

    // returns the currently active user
    getActiveUser() {
        return Meteor.users.findOne(this.userId, {
            fields: {
                profile: 1,
                emails: 1,
                about: 1,
                registrationNumber: 1,
                roles: 1
            }
        })
    },

    // updates email, name, and about fields of the current user
    updateUserProfile(data) {
        return Meteor.users.update(this.userId, {
            $set: {
                'emails.0.address': data.email,
                'profile.name': data.name,
                'profile.about': data.about
            }
        });
    },
})