import { Meteor } from 'meteor/meteor';

// meteor user related publications
Meteor.publish({
    // publishes the currently active user
    // if any any changes are made in the db, for this user
    // they will be reflected on the client side
    root() {
        return Meteor.users.find(this._id, {
            fields: {
                emails: 1,
                roles: 1,
                _id: 1,
                courses: {
                    name: 1,
                    completion: 1,
                    _id: 1
                }
            }
        });
    }
})