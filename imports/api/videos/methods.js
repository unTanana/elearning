import { Meteor } from 'meteor/meteor';

// video related meteor methods
Meteor.methods({
    // given a video url, deletes the file from aws storage
    deleteVideoFromStorage(fileUrl) {
        // gets file key
        const fileSplit = fileUrl.split('/');
        const key = fileSplit[fileSplit.length - 2] +  '/' + fileSplit[fileSplit.length - 1];

        // configures AWS credentials to access s3
        AWS.config.update({
            accessKeyId: Meteor.settings.amazon.ACCESS_KEY,
            secretAccessKey: Meteor.settings.amazon.SECRET_KEY
        });

        // sets the correct bucket and key parameters
        var s3 = new AWS.S3();
        var params = {
            Bucket: Meteor.settings.amazon.video_bucket,
            Key: key
        };
        
        // deletes file from storage
        s3.deleteObject(params, function (error, data) {
            if (error) {
                throw new Meteor.Error(error);
            } else {
                return data;
            }
        })
    },

    // given a file url, deletes it from aws storage
    deleteAttachmentFromStorage(fileUrl) {
        // gets file key
        const fileSplit = fileUrl.split('/');
        const key = fileSplit[fileSplit.length - 2] +  '/' + fileSplit[fileSplit.length - 1];

        // configures AWS credentials to access s3
        AWS.config.update({
            accessKeyId: Meteor.settings.amazon.ACCESS_KEY,
            secretAccessKey: Meteor.settings.amazon.SECRET_KEY
        });

        // sets the correct bucket and key parameters
        var s3 = new AWS.S3();
        var params = {
            Bucket: Meteor.settings.amazon.attachments_bucket,
            Key: key
        };

        // deletes file from storage
        s3.deleteObject(params, function (error, data) {
            if (error) {
                throw new Meteor.Error(error);
            } else {
                return data;
            }
        })
    },

    // given an image url, deletes the file from aws storage
    deleteThumbnailFromStorage(fileUrl) {
        // gets file key
        const fileSplit = fileUrl.split('/');
        const key = fileSplit[fileSplit.length - 2] +  '/' + fileSplit[fileSplit.length - 1];

        // sets the correct bucket and key parameters
        AWS.config.update({
            accessKeyId: Meteor.settings.amazon.ACCESS_KEY,
            secretAccessKey: Meteor.settings.amazon.SECRET_KEY
        });

        // sets the correct bucket and key parameters
        var s3 = new AWS.S3();
        var params = {
            Bucket: Meteor.settings.amazon.thumbnails_bucket,
            Key: key
        };

        s3.deleteObject(params, function (error, data) {
            if (error) {
                throw new Meteor.Error(error);
            } else {
                return data;
            }
        })
    },
})