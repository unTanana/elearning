import { Meteor } from 'meteor/meteor';
import Courses from './courses.js';
import DepGraphModule from 'dependency-graph';
const DepGraph = DepGraphModule.DepGraph;

Meteor.methods({
    // creates a course for the currently signed in professor
    createCourse(data) {
        const professorId = this.userId;
        data.professorId = professorId;
        return Courses.insert(data);
    },

    // edits data of a course for the currently signed in professor
    editCourse(data) {
        const professorId = this.userId;
        const { courseId } = data;
        delete data.courseId;

        Courses.update({
            _id: courseId,
            professorId
        }, {
                $set: data
            })
    },

    // deletes a course, and all related files from storage
    deleteCourse(courseId) {
        // get currently logged in professor
        const professorId = this.userId;

        // if no professorId, stop
        if (!professorId) {
            return;
        }

        // get course
        const course = Courses.findOne({
            _id: courseId,
            professorId
        });

        if (course) {
            if (course.courseThumbnail) {
                Meteor.call('deleteThumbnailFromStorage', course.courseThumbnail);
            }

            if (course.videos && course.videos.length) {
                course.videos.forEach(video => {
                    // if a lesson has a video url, delete it
                    if (video.url) {
                        Meteor.call('deleteVideoFromStorage', video.url);
                    }

                    // if a lesson has a thumbnail, delete it
                    if (video.thumbnail) {
                        Meteor.call('deleteThumbnailFromStorage', video.thumbnail);
                    }

                    // if a lesson has an attachment, delete it
                    if (video.attachment) {
                        Meteor.call('deleteAttachmentFromStorage', video.attachment);
                    }
                })
            }

            return Courses.remove({
                _id: courseId,
                professorId
            });
        }

        return true;
    },

    // retrieves courses of a professor (or the current logged in user)
    getCoursesForProfessor(professorId) {
        let targetId = professorId || this.userId;

        return Courses.find({
            professorId: targetId
        }).fetch();
    },

    // retrieves a course by it's id
    getCourseById(courseId) {
        const course = Courses.findOne(courseId);
        const professor = Meteor.users.findOne({ _id: course.professorId });
        // attach professor to the course object
        course.professor = professor;

        course.availablePrerequisites = Meteor.call('getAvailablePrerequisitesOfCourse', courseId);

        const loggedInStudent = Meteor.users.findOne({
            _id: this.userId,
            roles: 'student'
        });

        // if a student is logged in, retrieve the current progress on the course
        if (loggedInStudent) {
            const courseInStudent = loggedInStudent.courses && loggedInStudent.courses.find(course => (
                course._id === courseId
            ));

            if (courseInStudent) {
                course.progress = courseInStudent.completion
            }
        }

        return course;
    },

    // get filtered courses
    getCoursesData(filters) {
        const filtersObject = {};

        // process filters
        if (filters) {
            // categories
            if (filters.categories && filters.categories.length) {
                filtersObject.category = {
                    $in: filters.categories
                };
            }

            // levels
            if (filters.levels && filters.levels.length) {
                filtersObject.level = {
                    $in: filters.levels
                };
            }

            // search string
            if (filters.searchString) {
                filtersObject.$or = [
                    {
                        name: { '$regex': filters.searchString, '$options': 'i' }
                    },
                    {
                        name: { '$regex': filters.searchString, '$options': 'i' }
                    },
                    {
                        tags: filters.searchString
                    }
                ];
            }

            // pricing
            if (filters.priceMin || filters.priceMax) {
                const priceFilter = [];
                if (filters.priceMin) {
                    priceFilter.push({ price: { $gte: filters.priceMin } });
                }

                if (filters.priceMax) {
                    priceFilter.push({ price: { $lte: filters.priceMax } });
                }

                filtersObject.$and = priceFilter
            }
        }

        // get the courses
        const courses = Courses.find(filtersObject, {
            sort: {
                name: -1
            }
        }).fetch();

        // attach professors to courses
        // get all tags
        let tags = [];
        let professorIds = [];
        const professorToCourseMap = {};

        courses.forEach((course, index) => {
            tags = tags.concat(course.tags);
            professorIds.push(course.professorId);
            if (!professorToCourseMap[course.professorId]) {
                professorToCourseMap[course.professorId] = [index];
            } else {
                professorToCourseMap[course.professorId].push(index);
            }
        });

        tags = [... new Set(tags)];
        professorIds = [... new Set(professorIds)];

        let professors = Meteor.users.find({
            _id: {
                $in: professorIds
            },
            roles: 'professor'
        }).fetch();

        // match course to professor, and attach it to the course object
        professors.forEach(professor => {
            const indexMatches = professorToCourseMap[professor._id];
            indexMatches.forEach(index => {
                courses[index].professor = professor;
            });
        })

        return [courses, tags];
    },

    // calculate prerequisites graph
    getPrerequisitesGraph() {
        const graph = new DepGraph();

        // get all courses
        const courses = Courses.find({}, {
            sort: {
                name: -1
            }
        }).fetch();

        // populate nodes
        courses.forEach(course => graph.addNode(course._id));

        // populate dependencies
        courses.forEach(course => {
            if (course.prerequisites) {
                course.prerequisites.forEach(prerequisite => {
                    graph.addDependency(course._id, prerequisite);
                });
            }
        });

        return graph;
    },


    getAvailablePrerequisitesOfCourse(courseId) {
        // get the graph every time, as stuff might change
        const graph = Meteor.call('getPrerequisitesGraph');

        const courses = Courses.find({}, {
            sort: {
                name: -1
            }
        }).fetch();

        // get courses that depend on this course
        const dependants = graph.dependantsOf(courseId);

        // add the course itself to dependants, so it gets filtered out
        dependants.push(courseId);

        const availableCourses = courses.filter(course => dependants.indexOf(course._id) < 0);
        const availables = availableCourses.map(course => ({
            name: course.name,
            value: course._id
        }));

        // dependencies are left in, so they can be unchecked in necessary

        return availables;
    },

    // get all courses in id, name format
    getAllPrerequisites() {
        return Courses.find().fetch().map(course => ({
            name: course.name,
            value: course._id
        }));
    },

    // return courses with some public fields
    getCoursesCache() {
        return Courses.find({}, {
            fields: {
                _id: 1,
                name: 1,
                description: 1,
                tags: 1,
                professorId: 1
            }
        }).fetch();
    },

    checkStudentEligibility(courseId, studentId) {
        // check for currently logged in user, if an Id is not provided
        const idToCheck = studentId || this.userId;

        if (!idToCheck) {
            return {
                eligibility: false,
                reason: 'NOT_LOGGED_IN'
            }
        }

        const student = Meteor.users.findOne({
            _id: idToCheck
        });

        const course = Courses.findOne({
            _id: courseId
        });

        //eliminate case - student is already enrolled in this course
        if (student.courses && student.courses.map(course => course._id).indexOf(courseId) > -1) {
            return {
                eligibility: false,
                reason: 'ALREADY_ENROLLED'
            };
        }

        // eliminate case - student has no courses and the checked course has prerequisites
        if (!student.courses && course.prerequisites && course.prerequisites.length) {
            return {
                eligibility: false,
                reason: 'UNMET_PREREQUISITES'
            };
        }

        // eliminate case - checked course has no prerequisites
        if (!course.prerequisites || !course.prerequisites.length) {
            return {
                eligibility: true,
                reason: null
            };
        }

        // get completed courses
        const completedStudentCourses = student.courses.filter(course => {
            return course.isCompleted || course.completion >= 80;
        }).map(course => course._id);

        let hasUnmetPrerequisite = false;

        // compare them against prerequisites
        // if at least one prerequisite is not met, the student is not eligible
        course.prerequisites.some(courseId => {
            if (completedStudentCourses.indexOf(courseId) < 0) {
                hasUnmetPrerequisite = true;
                return true; // stops the loop
            }
        })

        return {
            eligibility: !hasUnmetPrerequisite,
            reason: hasUnmetPrerequisite ? 'UNMET_PREREQUISITES' : null
        };
    },

    enrollStudentToCourse(courseId, studentId) {
        // check for currently logged in user, if an Id is not provided
        const idToCheck = studentId || this.userId;

        const course = Courses.findOne({ _id: courseId });

        if (!course) {
            throw new Meteor.Error('COURSE NOT FOUND!');
        }

        const student = Meteor.users.findOne({
            _id: idToCheck
        });

        if (!student) {
            throw new Meteor.Error('STUDENT NOT FOUND!');
        }

        const activeCourses = student.courses || []

        activeCourses.push({
            _id: course._id,
            isCompleted: false,
            completion: 0,
            isSignedUpForTesting: false,
            hasPassedTest: false,
            videos: course.videos.map(video => ({
                name: video.name,
                isCompleted: false,
                completion: 0
            }))
        });

        const currentEnrollmentCount = course.enrollmentCount || 0;

        Courses.update(courseId, {
            $set: {
                enrollmentCount: currentEnrollmentCount + 1
            }
        })

        return Meteor.users.update({ _id: idToCheck }, {
            $set: {
                courses: activeCourses
            }
        })
    },

    getStudentCourses(studentId) {
        let targetId = studentId || this.userId;

        const student = Meteor.users.findOne(targetId);

        const studentCourses = student.courses;

        if (studentCourses) {
            const courseIds = studentCourses.map(course => course._id);
            const courses = Courses.find({
                _id: {
                    $in: courseIds
                }
            }).fetch();


            // attach professors to courses
            let professorIds = [];
            const professorToCourseMap = {};

            courses.forEach((course, index) => {
                professorIds.push(course.professorId);
                if (!professorToCourseMap[course.professorId]) {
                    professorToCourseMap[course.professorId] = [index];
                } else {
                    professorToCourseMap[course.professorId].push(index);
                }
            });

            professorIds = [... new Set(professorIds)];

            let professors = Meteor.users.find({
                _id: {
                    $in: professorIds
                },
                roles: 'professor'
            }).fetch();

            professors.forEach(professor => {
                const indexMatches = professorToCourseMap[professor._id];
                indexMatches.forEach(index => {
                    courses[index].professor = professor;
                });
            })

            return courses;
        } else {
            return [];
        }
    },

    updateLessonProgress(studentId, courseId, lessonIndex, progress) {
        const targetId = studentId || this.userId;

        const student = Meteor.users.findOne(targetId);
        const courses = student.courses;

        let lessonCompleted = false;
        let courseIndexOfCompletedLesson = null;

        courses.some((course, index) => {
            if (course._id === courseId) {
                course.videos[lessonIndex].completion = progress;
                if (progress >= 95) {
                    course.videos[lessonIndex].isCompleted = true;
                    lessonCompleted = true;
                    courseIndexOfCompletedLesson = index;
                }

                courses[index] = course;
                return true;
            }
        })

        let courseCompletion;
        if (lessonCompleted) {
            const lessonsCount = courses[courseIndexOfCompletedLesson].videos.length;
            const completedLessonsCount =
                courses[courseIndexOfCompletedLesson].videos.filter(video => video.isCompleted).length;
            courseCompletion = completedLessonsCount / lessonsCount * 100;

            courses[courseIndexOfCompletedLesson].completion = courseCompletion;

            if (courseCompletion === 100) {
                courses[courseIndexOfCompletedLesson].isCompleted = true;
            }
        }

        Meteor.users.update({
            _id: targetId
        }, {
                $set: {
                    courses
                }
            }
        );

        if (lessonCompleted) {
            return courseCompletion;
        }

        return false;

    }

})