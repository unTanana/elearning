// available course categories
const CATEGORIES = [
    {
        value: 'computer-science',
        title: 'Computer Science',
        disabled: false
    },
    {
        value: 'art',
        title: 'Art',
        disabled: false
    },
    {
        value: 'life',
        title: 'Life & Other Stuff',
        disabled: false
    },
    {
        value: 'soft-skills',
        title: 'Soft Skills',
        disabled: false
    },
    {
        value: 'finance',
        title: 'Finance',
        disabled: false
    }
];

// available course levels
const LEVELS = [
    {
        value: 'beginner',
        title: 'Beginner',
        disabled: false
    },
    {
        value: 'intermediate',
        title: 'Intermediate',
        disabled: false
    },
    {
        value: 'advanced',
        title: 'Advanced',
        disabled: false
    },
]

export { CATEGORIES, LEVELS };