import { Mongo } from 'meteor/mongo';

// creating the courses collection
const Courses = new Mongo.Collection('courses');
export default Courses;


/*

SCHEMA

_id
name
description
tags
professorId
items (videos or questions)
...
*/