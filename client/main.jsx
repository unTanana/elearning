import React from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';
import App from '/imports/App.jsx'

// render App component on startup, on the react-target element
Meteor.startup(() => {
    render(<App />, document.getElementById('react-target'));
});
